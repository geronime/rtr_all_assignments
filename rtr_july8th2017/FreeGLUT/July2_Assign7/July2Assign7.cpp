#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800, 600); //to declare initial window size
	glutInitWindowPosition(100, 100); //to declare initial window position
	glutCreateWindow("The Grid and Quad");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void display(void)
{

	void DrawGrid();
	void DrawQuad();

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/*Draw the grid*/
	DrawGrid();

	/*Draw the triangle on the grid*/
	DrawQuad();

	//to process buffered OpenGL Routines
	glFlush();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	// code
}

void DrawGrid() {
	/*Variable dclaraions for horizontal lines*/
	GLfloat glfX1Count = -1.0f;
	GLfloat glfX2Count = 1.0f;
	GLfloat glfYCount;

	/*Variable declarations for vertical lines*/
	GLfloat glfY1Count = 1.0f;
	GLfloat glfY2Count = -1.0f;
	GLfloat glfXCount;

	GLfloat glfZCount = 0.0f;

	/*Center horizontal red line*/
	glLineWidth(3.0);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();

	/*Center vertical green line*/
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glEnd();

	/*Equally spaced blue lines in both halves of screen(horizontally)*/
	glLineWidth(1.0);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	/*Upper half horizontal lines*/
	for (glfYCount = 0.048f; glfYCount < 1.0f; glfYCount += 0.048f) {
		glVertex3f(glfX1Count, glfYCount, glfZCount);
		glVertex3f(glfX2Count, glfYCount, glfZCount);
	}

	/*Lower half horizontal lines*/
	for (glfYCount = -0.048f; glfYCount > -1.0f; glfYCount -= 0.048f) {
		glVertex3f(glfX1Count, glfYCount, glfZCount);
		glVertex3f(glfX2Count, glfYCount, glfZCount);
	}
	glEnd();

	/*Equally spaced blue lines in both halves of screen(vertically)*/
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	/*Right half vertical lines*/
	for (glfXCount = 0.048f; glfXCount < 1.0f; glfXCount += 0.048f) {
		glVertex3f(glfXCount, glfY1Count, glfZCount);
		glVertex3f(glfXCount, glfY2Count, glfZCount);
	}

	/*Left half vertical lines*/
	for (glfXCount = -0.048f; glfXCount > -1.0f; glfXCount -= 0.048f) {
		glVertex3f(glfXCount, glfY1Count, glfZCount);
		glVertex3f(glfXCount, glfY2Count, glfZCount);
	}
	glEnd();
}

void DrawQuad() {
	/*Yellow bordered quad (not filled)*/
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glEnd();
}