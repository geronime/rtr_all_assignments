#include <GL/freeglut.h>
#include<math.h>

#define PI 3.1415926535898

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(600, 600); //to declare initial window size
	glutInitWindowPosition(100, 100); //to declare initial window position
	glutCreateWindow("The Kundali");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void display(void)
{
	void DrawLine(GLfloat, GLfloat, GLfloat, GLfloat);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/*Outer quad*/
	DrawLine(-0.5f, 0.5f, -0.5f, -0.5f);
	DrawLine(-0.5f, -0.5f, 0.5f, -0.5f);
	DrawLine(0.5f, -0.5f, 0.5f, 0.5f);
	DrawLine(0.5f, 0.5f, -0.5f, 0.5f);

	/*Inner quad*/
	DrawLine(-0.5f, 0.0f, 0.0f, 0.5f);
	DrawLine(0.0f, 0.5f, 0.5f, 0.0f);
	DrawLine(0.5f, 0.0f, 0.0f, -0.5f);
	DrawLine(0.0f, -0.5f, -0.5f, 0.0f);

	/*The diagonals*/
	DrawLine(-0.5f, 0.5f, 0.5f, -0.5f);
	DrawLine(0.5f, 0.5f, -0.5f, -0.5f);

	//to process buffered OpenGL Routines
	glFlush();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	// code
}

void DrawLine(GLfloat glX1Coordinate, GLfloat glY1Coordinate, GLfloat glX2Coordinate, GLfloat glY2Coordinate) {

	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(glX1Coordinate, glY1Coordinate, 0.0f);
	glVertex3f(glX2Coordinate, glY2Coordinate, 0.0f);
	glEnd();

}