#include <GL/freeglut.h>
#include<math.h>

#define PI 3.1415926535898

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(600, 600); //to declare initial window size
	glutInitWindowPosition(100, 100); //to declare initial window position
	glutCreateWindow("The Deathly Hallows Symbol");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void display(void)
{
	void DrawTriangle(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawOutlinedCircle(GLdouble, GLdouble, GLdouble);
	GLdouble CalculateLengthOfSide(GLdouble, GLdouble, GLdouble, GLdouble);
	void DrawInCircle(GLdouble, GLdouble, GLdouble);
	void DrawLine(GLdouble, GLdouble, GLdouble, GLdouble);

	glClear(GL_COLOR_BUFFER_BIT);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat x1 = 0.0f, x2 = -0.8f, x3 = 0.8f;
	GLfloat y1 = 0.8f, y2 = -0.8f, y3 = -0.8f;

	/*First draw the triangle*/
	DrawTriangle(x1, y1, x2, y2, x3, y3);

	/*Calculate the length of each side of the triangle*/
	GLdouble lengthOfSide1, lengthOfSide2, lengthOfSide3;
	lengthOfSide1 = CalculateLengthOfSide(x1, y1, x2, y2);
	lengthOfSide2 = CalculateLengthOfSide(x2, y2, x3, y3);
	lengthOfSide3 = CalculateLengthOfSide(x3, y3, x1, y1);

	/*Calculate the inCenter coordinates for the inner circle*/
	GLdouble Ox, Oy;
	Ox = ((lengthOfSide2 * (x1)) + (lengthOfSide3 * (x2)) + (lengthOfSide1 * (x3))) / (lengthOfSide1 + lengthOfSide2 + lengthOfSide3);
	Oy = ((lengthOfSide2 * (y1)) + (lengthOfSide3 * (y2)) + (lengthOfSide1 * (y3))) / (lengthOfSide1 + lengthOfSide2 + lengthOfSide3);

	/*Calculate the semiperimeter and area of the triangle to get the radius for inCircle*/
	GLdouble triangleSemiPerimeter, triangleArea;

	//Semiperimeter of triangle
	triangleSemiPerimeter = (lengthOfSide1 + lengthOfSide2 + lengthOfSide3) / 2;

	//Area of triangle
	triangleArea = sqrt(triangleSemiPerimeter*(triangleSemiPerimeter - lengthOfSide1)*(triangleSemiPerimeter - lengthOfSide2)*(triangleSemiPerimeter - lengthOfSide3));

	/*Calculate the inCircle radius using triangle's semiperimeter and area*/
	GLdouble inCircleRadius;
	inCircleRadius = triangleArea / triangleSemiPerimeter;

	/*Draw circle using the cordinates and radius calculated above*/
	DrawInCircle(Ox, Oy, inCircleRadius);

	/*Draw the center line*/
	DrawLine(x1, y1, (((x2)+(x3))) / 2, (((y2)+(y3))) / 2);

	//to process buffered OpenGL Routines
	glFlush();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	// code
}

void DrawTriangle(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3) {
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x2, y2, 0.0f);
	glVertex3f(x2, y2, 0.0f);
	glVertex3f(x3, y3, 0.0f);
	glVertex3f(x3, y3, 0.0f);
	glVertex3f(x1, y1, 0.0f);
	glEnd();
}

GLdouble CalculateLengthOfSide(GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2) {
	GLdouble dLengthOfSide;

	dLengthOfSide = sqrt(pow(((x1)-(x2)), 2.0) + pow(((y1)-(y2)), 2.0));

	return dLengthOfSide;
}

void DrawInCircle(GLdouble centerXCoordinate, GLdouble centerYCoordinate, GLdouble circleRadius) {
	int i;
	int iLineCount = 10000; //# of triangles used to draw circle
	GLdouble twicePi = 2.0f * PI;
	//GLdouble x = 0.0, y = -0.19, radius = 0.309017;

	glBegin(GL_LINE_LOOP);
	for (i = 0; i <= iLineCount; i++) {
		glVertex2f(
			centerXCoordinate + (circleRadius * cos(i *  twicePi / iLineCount)),
			centerYCoordinate + (circleRadius* sin(i * twicePi / iLineCount))
		);
	}
	glEnd();
}

void DrawLine(GLdouble startVertexX1, GLdouble startVertexY1, GLdouble endVertxX2, GLdouble endVertexY2) {
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(startVertexX1, startVertexY1, 0.0f);
	glVertex3f(endVertxX2, endVertexY2, 0.0f);
	glEnd();
}