#include <GL/freeglut.h>
#include<math.h>

#define PI 3.1415926535898

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(600, 600); //to declare initial window size
	glutInitWindowPosition(100, 100); //to declare initial window position
	glutCreateWindow("Concentric Circles");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void display(void)
{
	void DrawCircle(GLdouble, GLdouble, GLdouble);
	int iCount;
	GLdouble gldRadius = 0.1;
	glClear(GL_COLOR_BUFFER_BIT);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glLineWidth(1.0);

	int c = 0;
	GLfloat iColorsArray[10][3] = {
		{ 1.0f, 0.0f, 0.0f },
		{ 1.0f, 1.0f, 0.0f },
		{ 1.0f, 1.0f, 1.0f },
		{ 1.0f, 0.0f, 1.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.5f, 0.5f, 0.5f },
		{ 1.0f, 0.0f, 0.0f },
		{ 1.0f, 1.0f, 0.0f },
	};

	for (iCount = 0; iCount < 10; iCount++) {
		glColor3f(iColorsArray[iCount][c], iColorsArray[iCount][c + 1], iColorsArray[iCount][c + 2]);
		DrawCircle(0.0, 0.0, gldRadius);
		gldRadius += 0.1;
	}

	//to process buffered OpenGL Routines
	glFlush();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	// code
}

void DrawCircle(GLdouble centerXCoordinate, GLdouble centerYCoordinate, GLdouble circleRadius) {
	int i;
	int iLineCount = 50000; //# of triangles used to draw circle
	GLdouble twicePi = 2.0f * PI;	

	glBegin(GL_LINE_LOOP);
	for (i = 0; i <= iLineCount; i++) {
		glVertex2f(
			centerXCoordinate + (circleRadius * cos(i *  twicePi / iLineCount)),
			centerYCoordinate + (circleRadius* sin(i * twicePi / iLineCount))
		);
	}
	glEnd();
}