#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800, 600); //to declare initial window size
	glutInitWindowPosition(100, 100); //to declare initial window position
	glutCreateWindow("Horizontal Blue Lines"); 

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void display(void)
{

	//code

	//to clear all pixels
	glClear(GL_COLOR_BUFFER_BIT);

	GLfloat glfX1Count = -1.0f;
	GLfloat glfX2Count = 1.0f;
	GLfloat glfYCount;
	GLfloat glfZCount = 0.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/*Center horizontal red line*/
	glLineWidth(3.0);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();

	/*Equally spaced blue lines in both halves of screen(horizontally)*/
	glLineWidth(1.0);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	/*Upper half horizontal lines*/
	for (glfYCount = 0.048f; glfYCount < 1.0f; glfYCount += 0.048f) {
		glVertex3f(glfX1Count, glfYCount, glfZCount);
		glVertex3f(glfX2Count, glfYCount, glfZCount);
	}

	/*Lower half horizontal lines*/
	for (glfYCount = -0.048f; glfYCount > -1.0f; glfYCount -= 0.048f) {
		glVertex3f(glfX1Count, glfYCount, glfZCount);
		glVertex3f(glfX2Count, glfYCount, glfZCount);
	}
	glEnd();

	//to process buffered OpenGL Routines
	glFlush();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	// code
}