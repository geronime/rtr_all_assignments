#include <GL/freeglut.h>
#include<math.h>

#define PI 3.1415926535898

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(600, 600); //to declare initial window size
	glutInitWindowPosition(100, 100); //to declare initial window position
	glutCreateWindow("Concentric Rectangles");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat x1 = -0.5f, x2 = -0.5f, x3 = 0.5f, x4 = 0.5f;
	GLfloat y1 = 0.5f, y2 = -0.5f, y3 = -0.5f, y4 = 0.5f;

	GLfloat glfRedColorIndex = 0.0f, glfGreenColorIndex = 0.0f, glfBlueColorIndex = 0.0f;

	int i, c = 0;
	GLfloat iColorsArray[10][3] = {
		{ 1.0f, 0.0f, 0.0f },
		{ 1.0f, 1.0f, 0.0f },
		{ 1.0f, 1.0f, 1.0f },
		{ 1.0f, 0.0f, 1.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.5f, 0.5f, 0.5f },
		{ 1.0f, 0.0f, 0.0f },
		{ 1.0f, 1.0f, 0.0f },
	};

	/*Logic to draw 10 concentric triangles*/
	for (i = 0; i < 10; i++) {
		glColor3f(iColorsArray[i][c], iColorsArray[i][c + 1], iColorsArray[i][c + 2]);
		glBegin(GL_LINES);

		glVertex3f(x1, y1, 0.0f);
		glVertex3f(x2, y2, 0.0f);
		glVertex3f(x2, y2, 0.0f);
		glVertex3f(x3, y3, 0.0f);
		glVertex3f(x3, y3, 0.0f);
		glVertex3f(x4, y4, 0.0f);
		glVertex3f(x4, y4, 0.0f);
		glVertex3f(x1, y1, 0.0f);

		glEnd();

		/*Change these values to increase or decrease the distance between the triangles*/
		x1 -= 0.055f;
		y1 += 0.055f;
		x2 -= 0.055f;
		y2 -= 0.055f;
		x3 += 0.055f;
		y3 -= 0.055f;
		x4 += 0.055f;
		y4 += 0.055f;

		c = 0;

		glfRedColorIndex += 0.005f;
		glfGreenColorIndex += 0.1f;
		glfBlueColorIndex += 0.05f;
	}

	//to process buffered OpenGL Routines
	glFlush();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	// code
}