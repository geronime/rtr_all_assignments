#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Global declaration of the callback of the window
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//gloabal variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbEscapeKeyIsPressed = false;
bool gbIsActiveWindow = false;
bool gbFullscreen = false;

GLfloat angleRedLight = 0.0f;
GLfloat angleGreenLight = 0.0f;
GLfloat angleBlueLight = 0.0f;

GLboolean gbAnimateLightXAxis = GL_FALSE;
GLboolean gbAnimateLightYAxis = GL_FALSE;
GLboolean gbAnimateLightZAxis = GL_FALSE;

//GLfloat angle_Sphere = 0.0f;

//For LIGHT0
GLfloat light_ambient_light0[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light_diffuse_light0[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_specular_light0[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position_light0[] = { 0.0f, 0.0f, 0.0f, 0.0f };


//For LIGHT1
GLfloat light_ambient_light1[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light_diffuse_light1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_specular_light1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position_light1[] = { 0.0f, 0.0f, 0.0f, 0.0f };

//For LIGHT2
GLfloat light_ambient_light2[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light_diffuse_light2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_specular_light2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position_light2[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat light_model_ambient[] = { 0.2f, 0.2f, 0.2f, 0.0f };
GLfloat light_model_local_viewer[] = { 0.0f };

//Material arrays
//Emerald 1x1
GLfloat material_ambient_1x1[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat material_diffuse_1x1[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat material_specular_1x1[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat material_shininess_1x1 = 0.6f * 128;

//Jade 2x1
GLfloat material_ambient_2x1[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
GLfloat material_diffuse_2x1[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat material_specular_2x1[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
GLfloat material_shininess_2x1 = 0.1f * 128;

//Obsidian 3x1
GLfloat material_ambient_3x1[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat material_diffuse_3x1[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat material_specular_3x1[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat material_shininess_3x1 = 0.3f * 128;

//Pearl 4x1
GLfloat material_ambient_4x1[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat material_diffuse_4x1[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat material_specular_4x1[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat material_shininess_4x1 = 0.088f * 128;

//Ruby 5x1
GLfloat material_ambient_5x1[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat material_diffuse_5x1[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat material_specular_5x1[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat material_shininess_5x1 = 0.6f * 128;

//Turquoise 6x1
GLfloat material_ambient_6x1[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat material_diffuse_6x1[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat material_specular_6x1[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat material_shininess_6x1 = 0.1f * 128;

//Brass 1x2
GLfloat material_ambient_1x2[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat material_diffuse_1x2[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat material_specular_1x2[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat material_shininess_1x2 = 0.21794872f * 128;

//Bronze 2x2
GLfloat material_ambient_2x2[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
GLfloat material_diffuse_2x2[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat material_specular_2x2[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
GLfloat material_shininess_2x2 = 0.2f * 128;

//Chrome 3x2
GLfloat material_ambient_3x2[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat material_diffuse_3x2[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_specular_3x2[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat material_shininess_3x2 = 0.6f * 128;

//Copper 4x2
GLfloat material_ambient_4x2[] = { 0.19125f, 0.0735f, 0.225f, 1.0f };
GLfloat material_diffuse_4x2[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat material_specular_4x2[] = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
GLfloat material_shininess_4x2 = 0.1f * 128;

//Gold 5x2
GLfloat material_ambient_5x2[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
GLfloat material_diffuse_5x2[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
GLfloat material_specular_5x2[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat material_shininess_5x2 = 0.4f * 128;

//Silver 6x2
GLfloat material_ambient_6x2[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
GLfloat material_diffuse_6x2[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat material_specular_6x2[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat material_shininess_6x2 = 0.4f * 128;

//Black 1x3
GLfloat material_ambient_1x3[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse_1x3[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_specular_1x3[] = { 0.50f, 0.50f, 0.50f, 1.0f };
GLfloat material_shininess_1x3 = 0.25f * 128;

//Cyan 2x3
GLfloat material_ambient_2x3[] = { 0.0f, 0.1f, 0.06f, 1.0f };
GLfloat material_diffuse_2x3[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
GLfloat material_specular_2x3[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat material_shininess_2x3 = 0.3f * 128;

//Green 3x3
GLfloat material_ambient_3x3[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse_3x3[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat material_specular_3x3[] = { 0.45f, 0.55f, 0.45f, 1.0f };
GLfloat material_shininess_3x3 = 0.25f * 128;

//Red 4x3
GLfloat material_ambient_4x3[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse_4x3[] = { 0.5f, 0.0f, 0.0f, 1.0f };
GLfloat material_specular_4x3[] = { 0.7f, 0.6f, 0.6f, 1.0f };
GLfloat material_shininess_4x3 = 0.25f * 128;

//White 5x3
GLfloat material_ambient_5x3[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse_5x3[] = { 0.55f, 0.55f, 0.55f, 1.0f };
GLfloat material_specular_5x3[] = { 0.70f, 0.70f, 0.70f, 1.0f };
GLfloat material_shininess_5x3 = 0.25f * 128;

//Yellow Plastic 6x3
GLfloat material_ambient_6x3[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse_6x3[] = { 0.5f, 0.5f, 0.0f, 1.0f };
GLfloat material_specular_6x3[] = { 0.6f, 0.6f, 0.5f, 1.0f };
GLfloat material_shininess_6x3 = 0.25f * 128;

//Black 1x4
GLfloat material_ambient_1x4[] = { 0.02f, 0.02f, 0.02f, 1.0f };
GLfloat material_diffuse_1x4[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material_specular_1x4[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material_shininess_1x4 = 0.078125f * 128;

//Cyan 2x4
GLfloat material_ambient_2x4[] = { 0.0f, 0.05f, 0.05f, 1.0f };
GLfloat material_diffuse_2x4[] = { 0.4f, 0.5f, 0.5f, 1.0f };
GLfloat material_specular_2x4[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat material_shininess_2x4 = 0.078125f * 128;

//Green 3x4
GLfloat material_ambient_3x4[] = { 0.0f, 0.05f, 0.0f, 1.0f };
GLfloat material_diffuse_3x4[] = { 0.4f, 0.5f, 0.4f, 1.0f };
GLfloat material_specular_3x4[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat material_shininess_3x4 = 0.078125f * 128;

//Red 4x4
GLfloat material_ambient_4x4[] = { 0.05f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse_4x4[] = { 0.5f, 0.4f, 0.4f, 1.0f };
GLfloat material_specular_4x4[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat material_shininess_4x4 = 0.078125f * 128;

//White 5x4
GLfloat material_ambient_5x4[] = { 0.05f, 0.05f, 0.05f, 1.0f };
GLfloat material_diffuse_5x4[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat material_specular_5x4[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat material_shininess_5x4 = 0.078125f * 128;

//Yellow rubber 6x4
GLfloat material_ambient_6x4[] = { 0.05f, 0.05f, 0.0f, 1.0f };
GLfloat material_diffuse_6x4[] = { 0.5f, 0.5f, 0.4f, 1.0f };
GLfloat material_specular_6x4[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat material_shininess_6x4 = 0.078125f * 128;

GLUquadric *quadric = NULL;

bool gbLightEnabled = false;
//GLboolean gbShowSphere = GL_FALSE;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	//function prototype
	void initialize(void);
	void update(void);
	void display(void);
	void uninitialize(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	TCHAR szClassName[] = TEXT("OGLRTR");
	MSG msg;
	bool bDone = false;

	//code	
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering the class of window
	RegisterClassEx(&wndclass);

	//Creation of the actual window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("FFP: Final Light Assignment"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	//To enable the window
	ShowWindow(hwnd, nCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, hwnd, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;

				update();
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbIsActiveWindow = true;
		else
			gbIsActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //'F' Key
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		case 0x4C: //l or L
			if (gbLightEnabled == false) {
				gbLightEnabled = true;
				glEnable(GL_LIGHTING);
			}
			else {
				gbLightEnabled = false;
				glDisable(GL_LIGHTING);
			}
			break;

		case 0x58: //x or X	
			glEnable(GL_LIGHT0);
			glDisable(GL_LIGHT1);
			glDisable(GL_LIGHT2);
			gbAnimateLightXAxis = GL_TRUE;
			gbAnimateLightYAxis = GL_FALSE;
			gbAnimateLightZAxis = GL_FALSE;
			break;

		case 0x59: //y or Y
			glEnable(GL_LIGHT1);
			glDisable(GL_LIGHT0);
			glDisable(GL_LIGHT2);
			gbAnimateLightXAxis = GL_FALSE;
			gbAnimateLightYAxis = GL_TRUE;
			gbAnimateLightZAxis = GL_FALSE;
			break;

		case 0x5A: //z or Z	
			glEnable(GL_LIGHT2);
			glDisable(GL_LIGHT0);
			glDisable(GL_LIGHT1);
			gbAnimateLightXAxis = GL_FALSE;
			gbAnimateLightYAxis = GL_FALSE;
			gbAnimateLightZAxis = GL_TRUE;
			break;

		default:
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void initialize(void)
{
	//function prototype
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//code
	quadric = gluNewQuadric();

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	//Light0
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient_light0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse_light0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular_light0);
	glEnable(GL_LIGHT0);

	//Light1
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient_light1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse_light1);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular_light1);
	//glEnable(GL_LIGHT1);

	//Light2
	glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient_light2);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse_light2);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular_light2);
	//glEnable(GL_LIGHT2);	

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//function prototype
	void DrawSphere(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	/*************Light Animation based on key press**************/
	if (gbAnimateLightXAxis) {
		glRotatef(angleRedLight, 1.0f, 0.0f, 0.0f);
		light_position_light0[1] = angleRedLight;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position_light0);		
	}

	if (gbAnimateLightYAxis) {		
		glRotatef(angleGreenLight, 0.0f, 0.0f, 1.0f);
		light_position_light1[0] = angleGreenLight;
		glLightfv(GL_LIGHT1, GL_POSITION, light_position_light1);		
	}

	if (gbAnimateLightZAxis) {		
		glRotatef(angleBlueLight, 0.0f, 1.0f, 0.0f);
		light_position_light2[0] = angleBlueLight;
		glLightfv(GL_LIGHT2, GL_POSITION, light_position_light2);		
	}

	//The comment above each sphere denotes the position of sphere in row x column form
	/*************1st Column**************/
	//1x1		
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_1x1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_1x1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_1x1);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_1x1);
	glTranslatef(-2.5f, 2.4f, -10.0f);
	DrawSphere();

	//2x1
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_2x1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_2x1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_2x1);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_2x1);
	glTranslatef(-2.5f, 1.4f, -10.0f);
	DrawSphere();
	
	//3x1
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_3x1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_3x1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_3x1);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_3x1);
	glTranslatef(-2.5f, 0.4f, -10.0f);	
	DrawSphere();

	//4x1
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_4x1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_4x1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_4x1);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_4x1);
	glTranslatef(-2.5f, -0.6f, -10.0f);
	DrawSphere();

	//5x1
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_5x1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_5x1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_5x1);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_5x1);
	glTranslatef(-2.5f, -1.6f, -10.0f);
	DrawSphere();

	//6x1
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_6x1);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_6x1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_6x1);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_6x1);
	glTranslatef(-2.5f, -2.6f, -10.0f);
	DrawSphere();

	/*************2nd Column**************/
	//1x2
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_1x2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_1x2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_1x2);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_1x2);
	glTranslatef(-1.0f, 2.4f, -10.0f);
	DrawSphere();

	//2x2
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_2x2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_2x2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_2x2);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_2x2);
	glTranslatef(-1.0f, 1.4f, -10.0f);
	DrawSphere();

	//3x2
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_3x2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_3x2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_3x2);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_3x2);
	glTranslatef(-1.0f, 0.4f, -10.0f);
	DrawSphere();

	//4x2
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_4x2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_4x2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_4x2);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_4x2);
	glTranslatef(-1.0f, -0.6f, -10.0f);
	DrawSphere();

	//5x2
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_5x2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_5x2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_5x2);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_5x2);
	glTranslatef(-1.0f, -1.6f, -10.0f);
	DrawSphere();

	//6x2
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_6x2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_6x2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_6x2);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_6x2);
	glTranslatef(-1.0f, -2.6f, -10.0f);
	DrawSphere();

	/*************3rd Column**************/
	//1x3
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_1x3);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_1x3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_1x3);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_1x3);
	glTranslatef(0.5f, 2.4f, -10.0f);
	DrawSphere();

	//2x3
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_2x3);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_2x3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_2x3);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_2x3);
	glTranslatef(0.5f, 1.4f, -10.0f);
	DrawSphere();

	//3x3
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_3x3);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_3x3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_3x3);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_3x3);
	glTranslatef(0.5f, 0.4f, -10.0f);
	DrawSphere();

	//4x3
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_4x3);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_4x3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_4x3);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_4x3);
	glTranslatef(0.5f, -0.6f, -10.0f);
	DrawSphere();

	//5x3
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_5x3);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_5x3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_5x3);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_5x3);
	glTranslatef(0.5f, -1.6f, -10.0f);
	DrawSphere();

	//6x3
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_6x3);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_6x3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_6x3);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_6x3);
	glTranslatef(0.5f, -2.6f, -10.0f);
	DrawSphere();

	/*************4th Column**************/
	//1x4
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_1x4);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_1x4);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_1x4);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_1x4);
	glTranslatef(2.0f, 2.4f, -10.0f);
	DrawSphere();

	//2x4
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_2x4);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_2x4);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_2x4);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_2x4);
	glTranslatef(2.0f, 1.4f, -10.0f);
	DrawSphere();

	//3x4
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_3x4);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_3x4);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_3x4);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_3x4);
	glTranslatef(2.0f, 0.4f, -10.0f);
	DrawSphere();

	//4x4
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_4x4);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_4x4);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_4x4);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_4x4);
	glTranslatef(2.0f, -0.6f, -10.0f);
	DrawSphere();

	//5x4
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_5x4);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_5x4);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_5x4);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_5x4);
	glTranslatef(2.0f, -1.6f, -10.0f);
	DrawSphere();

	//6x4
	glLoadIdentity();
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient_6x4);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_6x4);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular_6x4);
	glMaterialf(GL_FRONT, GL_SHININESS, material_shininess_6x4);
	glTranslatef(2.0f, -2.6f, -10.0f);
	DrawSphere();
	
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 50.0f);
}

void update(void)
{
	//code
	angleRedLight = angleRedLight + 0.5f;
	if (angleRedLight >= 360.0f)
		angleRedLight = 0.0f;

	angleGreenLight = angleGreenLight + 0.5f;
	if (angleGreenLight >= 360.0f)
		angleGreenLight = 0.0f;

	angleBlueLight = angleBlueLight + 0.5f;
	if (angleBlueLight >= 360.0f)
		angleBlueLight = 0.0f;
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (quadric) {
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void DrawSphere(void) {
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	
	gluSphere(quadric, 0.45, 50, 50);
}