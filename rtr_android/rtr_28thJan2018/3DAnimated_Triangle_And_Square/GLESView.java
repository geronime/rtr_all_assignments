package com.geronime.opengles_3danimated_cube_pyramid;

import android.content.Context;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// For vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;


// A view for OpenGLES 3.2 that receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
	
	private final Context context;	
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_pyramid = new int[1];
	private int[] vao_cube = new int[1];
	private int[] vbo_position = new int[1];
	private int[] vbo_color = new int[1];
	private int mvpUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix
	
	private static float anglePyramid = 0.0f;
	private static float angleCube = 0.0f;
	
	public GLESView(Context drawingContext){
		super(drawingContext);
		
		context = drawingContext;
		
		// Set EGLContext to current supported version
		setEGLContextClientVersion(3);
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is a chnage in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false); // Handler of the gesture events
		gestureDetector.setOnDoubleTapListener(this); 
	}
	
	// Overriden method of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config){
		
		// OpenGLES verison check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("Geronime: GLES Version "+version);
		
		// GLSL version check
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("Geronime: GLSL Version "+glslVersion);
		
		initialize(gl);
	}
	
	// Overriden method of GLSurfaceView.Renderer
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height){
		resize(width, height);
	}
	
	// Overriden method of GLSurfaceView.Renderer
	@Override
	public void onDrawFrame(GL10 unused){
		draw();
	}
	
	//Handling the 'onTouch' event
	@Override
	public boolean onTouchEvent(MotionEvent event){
		int eventAction = event.getAction();
		
		if(!gestureDetector.onTouchEvent(event)){
			super.onTouchEvent(event);
		}
		
		return (true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override	
	public boolean onDoubleTap(MotionEvent e){
		System.out.println("Geronime: "+"Double Tap");
		return(true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e){
		// No code is required as we have already handled the event in onDoubleTap()
		return(true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e){
		System.out.println("Geronime: " + "Single Tap");
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onDown(MotionEvent e){
		// No code is required as we have already handled the event in onSingleTapConfirmed()
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
		System.out.println("Geronime: " + "Fling");
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public void onLongPress(MotionEvent e){
		System.out.println("Geronime: " + "Long Press");
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
		//uninitialize();
		//System.exit(0);
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public void onShowPress(MotionEvent e){
		
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e){
		return(true);
	}
	
	private void initialize(GL10 gl){
		/*************Vertex Shader Start*************/
		// Create Shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		// Vertex shader source 
		final String vertexShaderSourceCode = String.format(
		"#version 320 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec4 vColor;"+
		"out vec4 out_Color;"+
		"uniform mat4 u_mvp_matrix;"+
		"void main(void)"+
		"{"+
		"gl_Position = u_mvp_matrix * vPosition;"+
		"out_Color = vColor;"+
		"}"
		);
		
		// Provide the shader with the source code
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		// Compile the shader and check for error(if any)
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];		
		String sInfoLog = null;
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0){
				sInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("Geronime: Vertex shader compilation log = "+sInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		/*************Vertex Shader End*************/		
		
		/*************Fragment Shader Start*************/
		// Create shader
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		// Fragment shader source
		final String fragmentShaderSourceCode = String.format(
		"#version 320 es"+
		"\n"+
		"precision highp float;"+
		"in vec4 out_Color;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
		"FragColor = out_Color;"+
		"}"
		);
		
		// Provide the shader with source code
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		// Compile the shader and check for errors(if any)
        GLES32.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0] = 0; // Re-initialize
        iInfoLogLength[0] = 0; // Re-initialize
        sInfoLog=null; // Re-initialize
        
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        
		if (iShaderCompiledStatus[0] == GLES32.GL_FALSE) {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0) {
                sInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("Geronime: Fragment shader cmpilation log = "+sInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
		
		/*************Fragment Shader End*************/
		
		// Create shader program
		shaderProgramObject = GLES32.glCreateProgram();
		
		// Attach the vertex shader
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		
		// Attach the fragment shader
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		// Pre-link binding of shader program object with vertex shader position attribute
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.PND_ATTRIBUTE_VERTEX, "vPosition");
		
		// Pre-link binding of shader program object with vertex shader color attribute
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.PND_ATTRIBUTE_COLOR, "vColor");
		
		// Link both the shaders to shaderProgramObject
		GLES32.glLinkProgram(shaderProgramObject);
		
		int iShaderProgramLinkStatus[] = new int[1];
		iInfoLogLength[0] = 0; // Re-initialize
		sInfoLog = null; // Re-initialize
		
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0){
				sInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("Geronime: Shader program link log = "+sInfoLog);
				//uninitialize();
				//System.exit(0);
			}
		}
		
		// Get MVP uniform loacation
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		
		/***Initialization of vertices, colors, shader attribs, vbo(vertex buffer object), vao(vertex array object)****/
		final float pyramidVertices[] = new float[] {
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};
		
		final float pyramidColors[] = new float[] {
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f
		};
						
		final float cubeVertices[] = new float[] {
			// Front Face
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			// Back Face
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,

			// Right Face
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, 1.0f,

			// Left face
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,

			// Top Face
			- 1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,

			// Bottom Face
			-1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, 1.0f
		};
		
		final float cubeColors[] = new float[] {
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,

			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,

			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,

			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f
		};
				
		// Vertex Array Object
		/********Pyramid Vertex Array Object Start**********/
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);
		GLES32.glBindVertexArray(vao_pyramid[0]);
		
		/*******Vertex Buffer Object Position Start********/
		GLES32.glGenBuffers(1, vbo_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		
		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(pyramidVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(pyramidVertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            pyramidVertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.PND_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.PND_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		/*******Vertex Buffer Object Position End********/
		
		/*****Vertex Buffer Object Color Start******/
		GLES32.glGenBuffers(1, vbo_color, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
		
		byteBuffer=ByteBuffer.allocateDirect(pyramidColors.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(pyramidColors);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            pyramidColors.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.PND_ATTRIBUTE_COLOR,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.PND_ATTRIBUTE_COLOR);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		/*****Vertex Buffer Object Color End******/
        
		GLES32.glBindVertexArray(0);
		/********Pyramid Vertex Array Object End**********/
		
		/********Cube Vertex Array Object Start**********/
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
		/*******Vertex Buffer Object Position Start********/
		GLES32.glGenBuffers(1, vbo_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		
		byteBuffer=ByteBuffer.allocateDirect(cubeVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(cubeVertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            cubeVertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.PND_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.PND_ATTRIBUTE_VERTEX);        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        /*******Vertex Buffer Object Position End********/
		
		/*******Vertex Buffer Object Color Start********/
		GLES32.glGenBuffers(1, vbo_color, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
		
		byteBuffer=ByteBuffer.allocateDirect(cubeColors.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(cubeColors);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            cubeColors.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.PND_ATTRIBUTE_COLOR,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.PND_ATTRIBUTE_COLOR);        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        /*******Vertex Buffer Object Color End********/
		
			
		GLES32.glBindVertexArray(0);
		/********Cube Vertex Array Object End**********/
		

        // Enable depth test
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        
		// depth test to do
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        
		// Disable back face culling
        GLES32.glDisable(GLES32.GL_CULL_FACE);
        
		// Set the background frame color
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        // Set projectionMatrix to Identity matrix
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	
	}
	
	private void resize(int width, int height){
		
		// Adjust the viewport based on geometry changes, such as screen rotation
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/(float)height, 0.1f, 100.0f);
	}
	
	public void draw(){
		
		// Draw the background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		// Use shader program
		GLES32.glUseProgram(shaderProgramObject);
		
		// OpenGL-ES drawing
		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		
		/**************Pyramid drawing start**************/		
		// Set modelView, modelViewProjection and rotation matrices to identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		// Translate the pyramid to make it visible
		Matrix.translateM(modelViewMatrix, 0, -1.5f, 0.0f, -5.0f);
		
		// Rotate the pyramid
		Matrix.rotateM(rotationMatrix, 0, anglePyramid, 0.0f, 1.0f, 0.0f);
		
		// Multiply modelViewMatrix to rotationMatrix
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		
		// Multiply modelView and projectionMatrix to get modelViewProjectionMatrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        
        // Pass above modelViewProjection matrix to the vertex shader in 'u_mvp_matrix' shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
        
        // Bind vao
        GLES32.glBindVertexArray(vao_pyramid[0]);
        
        // Draw pyramid using glDrawArrays()
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
        
        // Unbind vao
        GLES32.glBindVertexArray(0);
		/**************Pyramid drawing end**************/		
		
		/**************Cube drawing start**************/		
		// Set modelView and modelViewProjection matrices to identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		// Translate the cube to make it visible
		Matrix.translateM(modelViewMatrix, 0, 1.5f, 0.0f, -5.5f);
		
		// Rotate the cube
		Matrix.rotateM(rotationMatrix, 0, angleCube, 1.0f, 1.0f, 1.0f);
		
		// Multiply modelViewMatrix to rotationMatrix
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		
		// Multiply modelView and projectionMatrix to get modelViewProjectionMatrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        
        // Pass above modelViewProjection matrix to the vertex shader in 'u_mvp_matrix' shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
        
        // Bind vao
        GLES32.glBindVertexArray(vao_cube[0]);
        
        // Draw cube using glDrawArrays()
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		
        // Unbind vao
        GLES32.glBindVertexArray(0);
		/**************Triangle drawing end**************/		
        
        // Un-use shader program
        GLES32.glUseProgram(0);
		
		// Call to update
		update();
		
        // Render/Flush
        requestRender();
	}
	
	void uninitialize()
    {
        // Destroy vao
        if(vao_pyramid[0] != 0) {
            GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
            vao_pyramid[0]=0;
        }
        
        // Destroy vbo
        if(vbo_position[0] != 0) {
            GLES32.glDeleteBuffers(1, vbo_position, 0);
            vbo_position[0]=0;
        }

        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0) {
                // Detach vertex shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                // Delete vertex shader object
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0) {
                // Detach fragment  shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // Delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // Delete shader program object
        if(shaderProgramObject != 0) {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
	
	private void update() {
	anglePyramid = anglePyramid + 1.0f;
	if (anglePyramid >= 360.0f) {
		anglePyramid = 0.0f;
	}

	angleCube = angleCube + 1.0f;
	if (angleCube >= 360.0f) {
		angleCube = 0.0f;
	}
}
}