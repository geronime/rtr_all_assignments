package com.geronime.opengles_window_with_eventhandling;

import android.content.Context;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// A view for OpenGLES 3.2 that receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
	
	private final Context context;	
	private GestureDetector gestureDetector;
	
	public GLESView(Context drawingContext){
		super(drawingContext);
		
		context = drawingContext;
		
		// Set EGLContext to current supported version
		setEGLContextClientVersion(3);
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is a chnage in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false); // Handler of the gesture events
		gestureDetector.setOnDoubleTapListener(this); 
	}
	
	// Overriden method of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config){
		
		// OpenGLES verison check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("Geronime: "+version);
		
		initialize(gl);
	}
	
	// Overriden method of GLSurfaceView.Renderer
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height){
		resize(width, height);
	}
	
	// Overriden method of GLSurfaceView.Renderer
	@Override
	public void onDrawFrame(GL10 unused){
		draw();
	}
	
	//Handling the 'onTouch' event
	@Override
	public boolean onTouchEvent(MotionEvent event){
		int eventAction = event.getAction();
		
		if(!gestureDetector.onTouchEvent(event)){
			super.onTouchEvent(event);
		}
		
		return (true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override	
	public boolean onDoubleTap(MotionEvent e){
		System.out.println("Geronime: "+"Double Tap");
		return(true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e){
		// No code is required as we have already handled the event in onDoubleTap()
		return(true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e){
		System.out.println("Geronime: " + "Single Tap");
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onDown(MotionEvent e){
		// No code is required as we have already handled the event in onSingleTapConfirmed()
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
		System.out.println("Geronime: " + "Fling");
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public void onLongPress(MotionEvent e){
		System.out.println("Geronime: " + "Long Press");
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
		System.out.println("Geronime: " + "Scroll");
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public void onShowPress(MotionEvent e){
		
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e){
		return(true);
	}
	
	private void initialize(GL10 gl){
		// Set the background frame color
		GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	}
	
	private void resize(int width, int height){
		
		// Adjust the viewport based on geometry changes, such as screen rotation
		GLES32.glViewport(0, 0, width, height);
	}
	
	public void draw(){
		
		// Draw the background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	}
}