package com.geronime.opengles_orthographic_triangle;

public class GLESMacros{
	public static final int PND_ATTRIBUTE_VERTEX = 0;
	public static final int PND_ATTRIBUTE_COLOR = 1;
	public static final int PND_ATTRIBUTE_NORMAL = 2;
	public static final int PND_ATTRIBUTE_TEXTURE = 3;
	
}