package com.geronime.opengles_3dtextured_cube_pyramid;

public class GLESMacros{
	public static final int PND_ATTRIBUTE_VERTEX = 0;
	public static final int PND_ATTRIBUTE_COLOR = 1;
	public static final int PND_ATTRIBUTE_NORMAL = 2;
	public static final int PND_ATTRIBUTE_TEXTURE0 = 3;
	
}