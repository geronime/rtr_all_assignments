package com.geronime.opengles_sphere_three_light;

import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix; // for Matrix math
import java.lang.*;

import java.nio.ShortBuffer; // for sphere

// A view for OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    
    private GestureDetector gestureDetector;
    
    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
	
	private float lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float lightDiffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
	private float lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	private float lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	private float lightAmbient1[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float lightDiffuse1[] = { 0.0f, 1.0f, 0.0f, 1.0f };
	private float lightSpecular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	private float lightPosition1[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	private float lightAmbient2[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float lightDiffuse2[] = { 0.0f, 0.0f, 1.0f, 1.0f };
	private float lightSpecular2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	private float lightPosition2[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	private float material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
	private float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
	private float material_shininess = 50.0f;
	
    private int  modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    private int  La_uniform, Ld_uniform, Ls_uniform, light_position_uniform;
	private int  La_uniform1, Ld_uniform1, Ls_uniform1, light_position_uniform1;
	private int  La_uniform2, Ld_uniform2, Ls_uniform2, light_position_uniform2;
    private int  Ka_uniform, Kd_uniform, Ks_uniform, material_shininess_uniform;
    
	Sphere sphere=new Sphere();
	
	float sphere_vertices[]=new float[1146];
	float sphere_normals[]=new float[1146];
	float sphere_textures[]=new float[764];
	short sphere_elements[]=new short[2280];
	
	int numVertices;
	int numElements;
	
	private int singleTapUniform;
    private int doubleTapUniform, u_perVertex, u_perFragment;

    private float perspectiveProjectionMatrix[]=new float[16]; // 4x4 matrix
           
	private int singleTap; // for toggling per-vertex and per-fragment shading
    private int doubleTap; // for lights
	private float gLightAngleY = 0.0f;
    
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        
        context=drawingContext;

        // accordingly set EGLContext to current supported version of OpenGL-ES
        setEGLContextClientVersion(3);

        // set Renderer for drawing on the GLSurfaceView
        setRenderer(this);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }
    
    // overriden method of GLSurfaceView.Renderer ( Init Code )
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // get OpenGL-ES version
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("Geronime: OpenGL-ES Version = "+glesVersion);
        // get GLSL version
        String glslVersion=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("Geronime: GLSL Version = "+glslVersion);

        initialize(gl);
    }
 
    // overriden method of GLSurfaceView.Renderer ( Chnge Size Code )
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }
    
    // Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        // code
        int eventaction = e.getAction();
        if(!gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // code
        doubleTap++;
        if(doubleTap > 1)
            doubleTap=0;
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        // code        
        singleTap++;
        if(singleTap > 2)
            singleTap=0;
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    private void initialize(GL10 gl)
    {
        // ***********************************************
        // Vertex Shader
        // ***********************************************
        // create shader
        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        
        // vertex shader source code
        final String vertexShaderSourceCode =String.format
        (
        "#version 320 es"+
        "\n"+
		"precision highp int;"+
		"uniform int u_perVertex, u_perFragment;"+
		"precision highp float;"+
        "in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+		
		"uniform int u_LKeyPressed;"+
		"uniform vec3 u_La, u_Ld, u_Ls;"+
		"uniform vec3 u_La1, u_Ld1, u_Ls1;"+
		"uniform vec3 u_La2, u_Ld2, u_Ls2;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform vec4 u_light_position, u_light_position1, u_light_position2;"+
		"uniform float u_material_shininess;"+
		"out vec3 out_phong_ads_color;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_direction, light_direction1, light_direction2;"+
		"out vec3 viewer_vector;"+
		 
		"uniform int u_double_tap;"+
		 
		 "vec3 calculateLight(vec3 u_La, vec3 u_Ld, vec3 u_Ls, vec4 u_light_position)"+
		"{"+
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"+
			"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
			"vec3 light_direction = normalize(vec3(u_light_position) - eyeCoordinates.xyz);"+
			"float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);"+
			"vec3 ambient = u_La * u_Kd;"+
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
			"vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
			"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);"+
			"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
			"out_phong_ads_color = ambient + diffuse + specular;"+
			"return out_phong_ads_color;"+
		"}"+
		 
		"void main(void)"+
		"{"+
		"if (u_double_tap == 1)"+
		"{" +
			"if(u_perVertex == 1)"+
			"{"+
				"vec3 light0 = calculateLight(u_La, u_Ld, u_Ls, u_light_position);"+
				"vec3 light1 = calculateLight(u_La1, u_Ld1, u_Ls1, u_light_position1);"+
				"vec3 light2 = calculateLight(u_La2, u_Ld2, u_Ls2, u_light_position2);"+
				"out_phong_ads_color += light0 + light1 + light2;"+
			"}"+

			"else if(u_perFragment == 1)"+
			"{"+
				"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
				"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
				"light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
				"light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;"+
				"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;"+
			"}"+

			"else"+
			"{"+
				"out_phong_ads_color = vec3(1.0, 1.0, 1.0);"+
			"}"+
		"}"+
		"else"+
		"{"+
			"out_phong_ads_color = vec3(1.0, 1.0, 1.0);"+
		"}"+
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}"
        );
        
        // provide source code to shader
        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
        
        // compile shader & check for errors
        GLES32.glCompileShader(vertexShaderObject);
        int[] iShaderCompiledStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;
        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("Geronime: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
           }
        }

        // ***********************************************
        // Fragment Shader
        // ***********************************************
        // create shader
        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        
        // fragment shader source code
        final String fragmentShaderSourceCode =String.format
        (
        "#version 320 es"+
        "\n"+
		"precision highp int;"+
		"uniform int u_perVertex;"+
		"uniform int u_perFragment;"+
        "precision highp float;"+		 
        "in vec3 transformed_normals;"+
		"in vec3 light_direction, light_direction1, light_direction2;"+
		"in vec3 viewer_vector;"+
		"in vec3 out_phong_ads_color;"+
		"out vec4 FragColor;"+
		"uniform vec3 u_La, u_Ld, u_Ls;"+
		"uniform vec3 u_La1, u_Ld1, u_Ls1;"+
		"uniform vec3 u_La2, u_Ld2, u_Ls2;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"uniform int u_double_tap;" +		
		
		"vec3 calculateLight(vec3 u_La, vec3 u_Ld, vec3 u_Ls, vec3 light_direction)"+
		"{"+
			"vec3 phong_ads_color;"+
			"vec3 normalized_transformed_normals=normalize(transformed_normals);"+
			"vec3 normalized_light_direction=normalize(light_direction);"+
			"vec3 normalized_viewer_vector=normalize(viewer_vector);"+
			"vec3 ambient = u_La * u_Ka;"+
			"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
			"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
			"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
			"phong_ads_color=ambient + diffuse + specular;"+
			"return phong_ads_color;"+
		"}"+
		
		"void main(void)"+
		"{"+
			"vec3 phong_ads_color;"+
			"if (u_double_tap == 1)"+
			"{"+
				"if(u_perVertex == 1)"+
				"{"+
					"FragColor = vec4(out_phong_ads_color, 1.0);"+
				"}"+

				"else if(u_perFragment == 1)"+
				"{"+
					"vec3 light0 = calculateLight(u_La, u_Ld, u_Ls, light_direction);"+
					"vec3 light1 = calculateLight(u_La1, u_Ld1, u_Ls1, light_direction1);"+
					"vec3 light2 = calculateLight(u_La2, u_Ld2, u_Ls2, light_direction2);"+
					"phong_ads_color += light0 + light1 + light2;"+
					"FragColor = vec4(phong_ads_color, 1.0f);"+
				"}"+

				"else"+
				"{"+
					"FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);"+
				"}"+
			"}"+

			"else"+
			"{"+
				"FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);"+
			"}"+
		"}"
        );
        
        // provide source code to shader
        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
        
        // compile shader and check for errors
        GLES32.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0] = 0; // re-initialize
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("Geronime: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
        
        // create shader program
        shaderProgramObject=GLES32.glCreateProgram();
        
        // attach vertex shader to shader program
        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        
        // attach fragment shader to shader program
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader attributes
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.PND_ATTRIBUTE_VERTEX,"vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.PND_ATTRIBUTE_NORMAL,"vNormal");

        // link the two shaders together to shader program object
        GLES32.glLinkProgram(shaderProgramObject);
        int[] iShaderProgramLinkStatus = new int[1];
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if (iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Geronime: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // get uniform locations
        modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");
		singleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_single_tap");
		u_perVertex =GLES32.glGetUniformLocation(shaderProgramObject, "u_perVertex");
		u_perFragment =GLES32.glGetUniformLocation(shaderProgramObject, "u_perFragment");
        
        /*******Uniform locations for Light*******/
		
		// Ambience color intensity of the light
		La_uniform =GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
		// Diffuse color intensity of the light
		Ld_uniform =GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
		// Specular color intensity of light
		Ls_uniform =GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
		// Light position
		light_position_uniform =GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");		

		// Ambience color intensity of the light
		La_uniform1 =GLES32.glGetUniformLocation(shaderProgramObject, "u_La1");
		// Diffuse color intensity of the light
		Ld_uniform1 =GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld1");
		// Specular color intensity of light
		Ls_uniform1 =GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls1");
		// Light position
		light_position_uniform1 =GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position1");

		// Ambience color intensity of the light
		La_uniform2 =GLES32.glGetUniformLocation(shaderProgramObject, "u_La2");
		// Diffuse color intensity of the light
		Ld_uniform2 =GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld2");
		// Specular color intensity of light
		Ls_uniform2 =GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls2");
		// Light position
		light_position_uniform2 =GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position2");

        Ka_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
        Kd_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
        Ks_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
        material_shininess_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");;

        // *** vertices, colors, shader attribs, vbo, vao initializations ***
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.PND_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.PND_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.PND_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.PND_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

        // enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        // depth test to do
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES32.glEnable(GLES32.GL_CULL_FACE);
        
        // Set the background color
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // black
        
        // initialization        
        doubleTap=0;
        
        // set projectionMatrix to identitu matrix
        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }
    
    private void resize(int width, int height)
    {
        // code
        GLES32.glViewport(0, 0, width, height);
        
        // calculate the projection matrix
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f); // typecasting is IMP
    }
    
    public void display()
    {
        // code
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        
		float lightYRotateZAxis = (float) (Math.cos(3.1415 * gLightAngleY / 180.0) * 10.0);
		float lightYRotateXAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);

		float lightXRotateZAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);
		float lightXRotateYAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);

		float lightZRotateYAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);
		float lightZRotateXAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);
		
        // use shader program
        GLES32.glUseProgram(shaderProgramObject);

        if(doubleTap==1)
        {
            GLES32.glUniform1i(doubleTapUniform, 1);
			
			if (singleTap==1) {
				GLES32.glUniform1i(u_perVertex, 1);
				GLES32.glUniform1i(u_perFragment, 0);
			}

			else if (singleTap==2) {
				GLES32.glUniform1i(u_perFragment, 1);
				GLES32.glUniform1i(u_perVertex, 0);
			}
            
            lightPosition[0] = 0.0f;
			lightPosition[1] = lightXRotateYAxis;
			lightPosition[2] = lightXRotateZAxis;
			lightPosition[3] = 1.0f;

			lightPosition1[0] = lightYRotateXAxis;
			lightPosition1[1] = 0.0f;
			lightPosition1[2] = lightYRotateZAxis;
			lightPosition1[3] = 1.0f;

			lightPosition2[0] = lightZRotateXAxis;
			lightPosition2[1] = lightZRotateYAxis;
			lightPosition2[2] = 0.0f;
			lightPosition2[3] = 1.0f;

			// Set light properties
			GLES32.glUniform3fv(La_uniform, 1, lightAmbient, 0);
			GLES32.glUniform3fv(Ld_uniform, 1, lightDiffuse, 0);
			GLES32.glUniform3fv(Ls_uniform, 1, lightSpecular, 0);
			GLES32.glUniform4fv(light_position_uniform, 1, lightPosition, 0);

			GLES32.glUniform3fv(La_uniform1, 1, lightAmbient1, 0);
			GLES32.glUniform3fv(Ld_uniform1, 1, lightDiffuse1, 0);
			GLES32.glUniform3fv(Ls_uniform1, 1, lightSpecular1, 0);
			GLES32.glUniform4fv(light_position_uniform1, 1, lightPosition1, 0);

			GLES32.glUniform3fv(La_uniform2, 1, lightAmbient2, 0);
			GLES32.glUniform3fv(Ld_uniform2, 1, lightDiffuse2, 0);
			GLES32.glUniform3fv(Ls_uniform2, 1, lightSpecular2, 0);
			GLES32.glUniform4fv(light_position_uniform2, 1, lightPosition2, 0);

			// Set material properties
			GLES32.glUniform3fv(Ka_uniform, 1, material_ambient, 0);
			GLES32.glUniform3fv(Kd_uniform, 1, material_diffuse, 0);
			GLES32.glUniform3fv(Ks_uniform, 1, material_specular, 0);
			GLES32.glUniform1f(material_shininess_uniform, material_shininess);
        }
        else
        {
            GLES32.glUniform1i(doubleTapUniform, 0);
        }
        
        // OpenGL-ES drawing
        float modelMatrix[]=new float[16];
        float viewMatrix[]=new float[16];
        
        // set modelMatrix and viewMatrix matrices to identity matrix
        Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,0.0f,0.0f,-1.5f);
        
        GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
        
        // bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);
        
        // un-use shader program
        GLES32.glUseProgram(0);
        
		gLightAngleY = gLightAngleY + 1.0f;

		if (gLightAngleY >= 360.0f)
			gLightAngleY = gLightAngleY - 360.0f;
		
        // render/flush
        requestRender();
    }
    
    void uninitialize()
    {
        // code
        // CUBE
        // destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }

        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}
