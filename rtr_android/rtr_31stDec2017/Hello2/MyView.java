package com.geronime.hello2;

import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;
import android.graphics.Color;

public class MyView extends TextView {
	
	MyView(Context context) {
		super(context);
		
		this.setText("Hello World 2");
		this.setTextSize(60);
		this.setTextColor(Color.rgb(0, 255, 0));
		this.setGravity(Gravity.CENTER);
	}
}