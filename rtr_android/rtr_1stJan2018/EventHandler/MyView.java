package com.geronime.eventhandling;

import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

public class MyView extends TextView implements OnGestureListener, OnDoubleTapListener{
	
	private GestureDetector gestureDetector;
	
	MyView(Context context) {
		super(context);
		
		this.setText("Hello World!!!");
		this.setTextSize(60);
		this.setTextColor(Color.rgb(0, 255, 0));
		this.setGravity(Gravity.CENTER);
		
		gestureDetector = new GestureDetector(context, this, null, false);
	}
	
	//Handling the 'onTouch' event
	@Override
	public boolean onTouchEvent(MotionEvent event){
		int eventAction = event.getAction();
		
		if(!gestureDetector.onTouchEvent(event)){
			super.onTouchEvent(event);
		}
		
		return (true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override	
	public boolean onDoubleTap(MotionEvent e){
		setText("Double Tap");
		return(true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e){
		// No code is required as we have already handled the event in onDoubleTap()
		return(true);
	}
	
	// Abstract method from onDoubleTapListener and hence need to be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e){
		setText("Single Tap");
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onDown(MotionEvent e){
		// No code is required as we have already handled the event in onSingleTapConfirmed()
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
		setText("Fling");
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public void onLongPress(MotionEvent e){
		setText("Long Press");
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
		setText("Scroll");
		return(true);
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public void onShowPress(MotionEvent e){
		
	}
	
	// Abstract method from onGestureListener and hence need to be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e){
		return(true);
	}
}