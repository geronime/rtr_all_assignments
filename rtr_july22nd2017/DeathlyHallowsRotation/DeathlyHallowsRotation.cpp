#include<windows.h>
#include<GL/gl.h>
#include<math.h>

#define WIN_WIDTH 600
#define WIN_HEIGHT 600
#define PI 3.1415926535898

#pragma comment(lib, "opengl32.lib")

void DrawTriangle(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
void DrawOutlinedCircle(GLdouble, GLdouble, GLdouble);
GLdouble CalculateLengthOfSide(GLdouble, GLdouble, GLdouble, GLdouble);
void DrawInCircle(GLdouble, GLdouble, GLdouble);
void DrawLine(GLdouble, GLdouble, GLdouble, GLdouble);

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global Variable declaration
HDC ghdc;
HGLRC ghglrc;
HWND ghwnd;
GLfloat angleTri = 0.0f;
GLfloat angleCirc = 0.0f;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbFullscreen = false;
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	void Initialize(void);
	void display();
	void Uninitialize(void);
	void update(void);
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClsName[] = TEXT("GL_TRIANGLES_LINES");
	bool bDone = false;

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szClsName;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	if (!RegisterClassEx(&wndClass)) {
		MessageBox(NULL, TEXT("Class Not Register Successfully\n Exiting.."), TEXT("ERROR"), MB_ICONERROR);
		exit(0);
	}

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClsName,
		TEXT("Deathly Hallows Symbol (mathematically)"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (hwnd == NULL) {
		MessageBox(NULL, TEXT("Failed To Create Window \n Exiting.."), TEXT("ERROR"), MB_ICONERROR);
		exit(0);
	}

	ghwnd = hwnd;

	Initialize();
	ShowWindow(hwnd, nCmdShow);

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT)
				bDone = true;
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) {
				if (gbEscapeKeyIsPressed == true)
					bDone = true;

				/*Rotate Deathly Hallows*/
				update();

				display();
			}
		}
	}

	Uninitialize();
	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	void MakeFullscreen(void);
	void LeaveFullscreen(void);
	void Uninitialize(void);
	void display(void);
	void resize(int, int);

	switch (iMsg) {
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam) {
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // 'f' OR 'F'
			if (gbFullscreen == false) {
				MakeFullscreen();
				gbFullscreen = true;
			}
			else {
				LeaveFullscreen();
				gbFullscreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void Initialize(void) {
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormat = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);
	iPixelFormat = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormat == 0) {
		MessageBox(ghwnd, TEXT("Failed To Choose Pixel Format"), TEXT("ERROR"), MB_ICONERROR);
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormat, &pfd) == FALSE) {
		MessageBox(ghwnd, TEXT("Failed To Set Pixel Format"), TEXT("ERROR"), MB_ICONERROR);
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if ((ghglrc = wglCreateContext(ghdc)) == NULL) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghglrc) == FALSE) {
		wglDeleteContext(ghglrc);
		ghglrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat x1 = 0.0f, x2 = -0.5f, x3 = 0.5f;
	GLfloat y1 = 0.5f, y2 = -0.5f, y3 = -0.5f;

	/*First draw the triangle*/
	DrawTriangle(x1, y1, x2, y2, x3, y3);

	/*Calculate the length of each side of the triangle*/
	GLdouble lengthOfSide1, lengthOfSide2, lengthOfSide3;
	lengthOfSide1 = CalculateLengthOfSide(x1, y1, x2, y2);
	lengthOfSide2 = CalculateLengthOfSide(x2, y2, x3, y3);
	lengthOfSide3 = CalculateLengthOfSide(x3, y3, x1, y1);

	/*Calculate the inCenter coordinates for the inner circle*/
	GLdouble Ox, Oy;
	Ox = ((lengthOfSide2 * (x1)) + (lengthOfSide3 * (x2)) + (lengthOfSide1 * (x3))) / (lengthOfSide1 + lengthOfSide2 + lengthOfSide3);
	Oy = ((lengthOfSide2 * (y1)) + (lengthOfSide3 * (y2)) + (lengthOfSide1 * (y3))) / (lengthOfSide1 + lengthOfSide2 + lengthOfSide3);

	/*Calculate the semiperimeter and area of the triangle to get the radius for inCircle*/
	GLdouble triangleSemiPerimeter, triangleArea;

	//Semiperimeter of triangle
	triangleSemiPerimeter = (lengthOfSide1 + lengthOfSide2 + lengthOfSide3) / 2;

	//Area of triangle
	triangleArea = sqrt(triangleSemiPerimeter*(triangleSemiPerimeter - lengthOfSide1)*(triangleSemiPerimeter - lengthOfSide2)*(triangleSemiPerimeter - lengthOfSide3));

	/*Calculate the inCircle radius using triangle's semiperimeter and area*/
	GLdouble inCircleRadius;
	inCircleRadius = triangleArea / triangleSemiPerimeter;

	/*Draw circle using the cordinates and radius calculated above*/
	DrawInCircle(Ox, Oy, inCircleRadius);
	glRotatef(angleCirc, 0.0f, 1.0f, 0.0f); //Rotate the circle

	/*Draw the center line*/
	DrawLine(x1, y1, (((x2)+(x3))) / 2, (((y2)+(y3))) / 2);

	SwapBuffers(ghdc);
}

void resize(int iWidth, int iHeight) {
	if (iHeight == 0)
		iHeight = 1;

	glViewport(0, 0, (GLsizei)iWidth, (GLsizei)iHeight);
}

void MakeFullscreen(void) {
	MONITORINFO mi = { sizeof(MONITORINFO) };

	dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

	if (dwStyle & WS_OVERLAPPEDWINDOW) {
		if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITOR_DEFAULTTOPRIMARY), &mi)) {
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
		}
	}
	ShowCursor(FALSE);
}

void LeaveFullscreen(void) {
	SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
	SetWindowPlacement(ghwnd, &wpPrev);
	SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
	ShowCursor(TRUE);
}

void Uninitialize(void) {
	if (gbFullscreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghglrc);
	ghglrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
}

void DrawTriangle(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3) {
	glRotatef(angleCirc, 0.0f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x2, y2, 0.0f);
	glVertex3f(x2, y2, 0.0f);
	glVertex3f(x3, y3, 0.0f);
	glVertex3f(x3, y3, 0.0f);
	glVertex3f(x1, y1, 0.0f);
	glEnd();
}

GLdouble CalculateLengthOfSide(GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2) {
	GLdouble dLengthOfSide;

	dLengthOfSide = sqrt(pow(((x1)-(x2)), 2.0) + pow(((y1)-(y2)), 2.0));

	return dLengthOfSide;
}

void DrawInCircle(GLdouble centerXCoordinate, GLdouble centerYCoordinate, GLdouble circleRadius) {
	int i;
	int iLineCount = 10000; //# of triangles used to draw circle
	GLdouble twicePi = 2.0f * PI;
	//GLdouble x = 0.0, y = -0.19, radius = 0.309017;	

	glBegin(GL_LINE_LOOP);
	for (i = 0; i <= iLineCount; i++) {
		glVertex2f(
			centerXCoordinate + (circleRadius * cos(i *  twicePi / iLineCount)),
			centerYCoordinate + (circleRadius* sin(i * twicePi / iLineCount))
		);
	}
	glEnd();
}

void DrawLine(GLdouble startVertexX1, GLdouble startVertexY1, GLdouble endVertxX2, GLdouble endVertexY2) {
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(startVertexX1, startVertexY1, 0.0f);
	glVertex3f(endVertxX2, endVertexY2, 0.0f);
	glEnd();
}

void update() {
	angleTri = angleTri + 0.08f;
	if (angleTri >= 360) {
		angleTri = 0.0f;
	}

	angleCirc = angleCirc + 0.08f;
	if (angleCirc >= 360) {
		angleCirc = 0.0f;
	}
}