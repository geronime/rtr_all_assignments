// Windows headers
#include <Windows.h>

//OpenGL headers
#include< gl/GL.h>
#include <gl/GLU.h>

//C headers
#include <stdio.h>
#include <stdlib.h>

//C++ headers
#include <vector>

//Symbolic Constants

#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE			256		//Maximum length of string in mesh file
#define S_EQUAL				0		//return value of strcmp() when strings are equal

#define WIN_INIT_X			100		//X coordinate to top-left point
#define WIN_INIT_Y			100		//Y coordinate to top-left point
#define WIN_WIDTH			800		//Initial width of window
#define WIN_HEIGHT			600		//Initial heigth of window

#define VK_F 0x46 //Virtual key code for capital F
#define VK_f 0x60 //Virtual key code for dmall f

#define NR_POINT_COORDS 3 //Number of point coordinates
#define NR_TEXTURE_COORDS 2 //Number of texture coordinates
#define NR_NORMAL_COORDS 3 //Number of normal coordinates
#define NR_FACE_TOKENS 3 //Number of face tokens

#define FOY_ANGLE 45 //Field of view in Y direction
#define ZNEAR 0.1 //Distance from viewer to near plane of viewing volume
#define ZFAR 200.0 //Distance from viewer to far plane of viewing volume

#define VIEWPORT_BOTTOMLEFT_X 0 //X-coordinate of bottom-left point of the viewport rectangle
#define VIEWPORT_BOOTOMLEFT_Y 0 //Y-coordinate of bottom-left point of the viewport rectangle

#define MONKEYHEAD_X_TRANSLATE 0.0f //X tranlation of monkey head
#define MONKEYHEAD_Y_TRANSLATE -0.0f //Y tranlation of monkey head
#define MONKEYHEAD_Z_TRANSLATE -5.0f //Z tranlation of monkey head

#define MONKEYHEAD_X_SCALE_FACTOR 1.5f //X-Scale factor of monkeyhead
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f //Y-Scale factor of monkeyhead
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f //Z-Scale factor of monkeyhead

#define START_ANGLE_POS 0.0f //Marks begining angle position of monkeyhead
#define END_ANGLE_POS 360.0f //Marks terminating angle position of monkeyhead
#define MONKEYHEAD_ANGLE_INCREMENT 0.5f // Increment angle of monkeyhead

//Import libraries
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Error handling macros
#define ERRORBOX1(lpszErrorMessage, lpszCaption){MessageBox((HWND)NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR); ExitProcess(EXIT_FAILURE);}

#define ERRORBOX2(hWnd, lpszErrorMessage, lpszCaption){MessageBox((HWND)NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR);ExitProcess(EXIT_FAILURE);}

//Callback procedure declaration
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

//Global definitions

HWND g_hwnd = NULL; //Handle to a window
HDC g_hdc = NULL; //Handle to a device context
HGLRC g_hrc = NULL; // Handle to OpenGL rendering context

DWORD g_dwStyle = NULL; //Window style
WINDOWPLACEMENT	g_wpPrev; //Structure for holding previous window position

bool g_bActiveWindow = false; //Flag for indicating whether window is active or not
bool g_bEscapeKeyIsPressed = false; //Flag for indicating whether escape is pressed or not
bool g_bFullScreen = false; //Flag indicating whether window currently fullscreen or not
bool gbLightEnabled = false; // Flag indicating lighting turned on or off

GLfloat g_rotate;

// Lighting data
GLfloat light_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess[] = { 50.0f };

//Vector of vector of floats to hold vertices data
std::vector<std::vector<float>> g_vertices;

//Vector of vector of floats to hold texture data
std::vector<std::vector<float>> g_texture;

//Vector of vector of floats to hold normal data
std::vector<std::vector<float>> g_normals;

//Vector of vector of int to hold index data in g_vertices
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;

// Handle to a mesh file
FILE *g_fp_meshfile = NULL;

// Handle to a log file
FILE *g_fp_logfile = NULL;

// Hold line in a file
char line[BUFFER_SIZE];

// Definition of a window entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevinstance, LPSTR lpCmdLine, int nShowCmd)
{
	// Function prototypes user defined functions called within WinMain
	void initialize(void);	//	Initialize OpenGL state machine
	void update(void);	//	Update state variables
	void display(void);	//	Render scene
	void uninitialize(void);	//	Free and Destroy resources

	// WinMain : Local data definitions
	static TCHAR szAppName[] = TEXT("Mesh loading version 2"); // Name of window class
	HWND	hWnd = NULL;	// Handle to a window
	HBRUSH hBrush = NULL;	// Handle to a background painting brush
	HCURSOR hCursor = NULL;	// Handle to a cursor
	HICON hIcon = NULL;	// Handle to an icon
	HICON hIconSm = NULL;	// Handle to a small icon

	bool	bDone = false; // Flag indicating whether or not to exit from game loop
	WNDCLASSEX wndEx; // Structure holding window class attributes
	MSG	msg;	// Structure holding message attributes

	// Zero out structures ZeroMemory((void*)&wndEx, sizeof(WNDCLASSEX)); ZeroMemory((void*)&msg, sizeof(MSG));
	// Acquire following resources :
	// 1	Black brush
	// 2	Cursor
	// 3	Icon
	// 4	Small Icon

	hBrush = (HBRUSH)GetStockObject(BLACK_BRUSH);
	if (!hBrush)
		ERRORBOX1("Error in getting stock object", "GetStockObject Error");

	hCursor = LoadCursor((HINSTANCE)NULL, IDC_ARROW);
	if (!hCursor)
		ERRORBOX1("Error in loading a cursor", "LoadCursor Error");

	hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION); if (!hIcon)
		ERRORBOX1("Error in loading icon", "Loadlcon Error");

	hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if (!hIconSm)
		ERRORBOX1("Error in loading icon", "Loadlcon Error");

	// Fill window class structure
	wndEx.cbClsExtra = 0; //Extra class bytes allocation
	wndEx.cbWndExtra = 0; //Extra window butes allocation
	wndEx.cbSize = sizeof(WNDCLASSEX); //size of structure
	wndEx.hbrBackground = hBrush; //Handle to a background brush
	wndEx.hCursor = hCursor; //Handle to a cursor
	wndEx.hIcon = hIcon; //Handle to a icon
	wndEx.hIconSm = hIconSm; //Handle to a small icon
	wndEx.hInstance = hInstance; //Handle to a program instance
	wndEx.lpfnWndProc = WndProc; //Address of a window procedure
	wndEx.lpszClassName = szAppName; //Name of a class
	wndEx.lpszMenuName = NULL; //Name of a menu
	wndEx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //Window style

	//Registering a window class
	if (!RegisterClassEx(&wndEx)) {
		ERRORBOX1("Error in registering a class", "Register class error");
	}

	//Create window in memory
	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		szAppName,
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		WIN_INIT_X,
		WIN_INIT_Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		(HWND)NULL,
		(HMENU)NULL,
		hInstance,
		(LPVOID)NULL);

	if (!hWnd) {
		ERRORBOX1("Error in creating window in memory", "CreateWindowEx error");
	}

	//Store handle to window in global handle
	g_hwnd = hWnd;

	//Initialize OpenGL renderinf context
	initialize();

	ShowWindow(hWnd, SW_SHOW); //Set specified window's show state
	SetForegroundWindow(hWnd); //Put a thread created specified window in the foreground
	SetFocus(hWnd); //Set keyboard focus to specified window

	//Game loop
	while (!bDone) {
		//Remark: PeekMessage() is not blocking a function
		if (PeekMessage(&msg, //Pointer to structure for window message
			(HWND)NULL, //Handle to a window
			0, //First message in a queue
			0, //Last message in queue (select all messages)
			PM_REMOVE)) //Remove message from queue after processing  from PeekMessage
		{
			if (msg.message == WM_QUIT) { //If current message is a quit message then exit game loop
				bDone = true; //Causes game loop too exit
			}
			else {
				TranslateMessage(&msg); //Translate virtual key message into character message
				DispatchMessage(&msg); //Dispatches message to window procedure
			}
		}
		else {	//If there is no message in the message queue then do rendering
			if (g_bActiveWindow == true) {
				if (g_bEscapeKeyIsPressed) {
					bDone = true;
				}
				else {
					//Update state variables
					update();
					//Render the scene
					display();
				}
			}
		}
	}

	//Release and destroy resources
	uninitialize();

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int); //handle window resize event
	void ToggleFullscreen(void); //Toggle fullscreen

	void uninitialize(void); //Release or destroy resources

	//code
	switch (uMsg)
	{

	//Handler of an event: Activate or deactivate a window
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			g_bActiveWindow = true;
		else
			g_bActiveWindow = false;
		break;

	//Handler of an event: Background of a window must be erased
	case WM_ERASEBKGND:
		return(0);

	//Handler of an event: A window has been resizeed
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	//Handler of an event: A key has been pressed
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		//Handler of an event: Escape key is pressed
		case VK_ESCAPE:
			g_bEscapeKeyIsPressed = true;
			break;
		case VK_F: //'F' Key
		case VK_f: //'f' Key
			if (g_bFullScreen == false)
			{
				ToggleFullscreen();
				g_bFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				g_bFullScreen = false;
			}
			break;

		case 0x4C: //l or L
			if (gbLightEnabled == false) {
				gbLightEnabled = true;
				glEnable(GL_LIGHTING);
			}
			else {
				gbLightEnabled = false;
				glDisable(GL_LIGHTING);
			}
			break;

		//For all other keys
		default:
			break;
		}
		break;
	
	//Handler of an event: Left mouse button has been pressed
	case WM_LBUTTONDOWN:
		break;

	//Handler of an event: A window application must be closed
	case WM_CLOSE:
		uninitialize();
		break;

	//Handler of an event: A window is being destroyed
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	//Default processing of events
	return(DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi; //Information structure holding monitor information

	//code
	if (g_bFullScreen == false)
	{
		//Screen is currently not full screen, so toggle the fullscreen mode

		//Retrieves specified info of specified window
		//hWnd : which window? GWL_STYLE : Get window style 
		g_dwStyle = GetWindowLong(g_hwnd, GWL_STYLE);
		if (g_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			// If window is currently having WS_OVERLAPPEDWINDOW style
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(g_hwnd, &g_wpPrev) && GetMonitorInfo(MonitorFromWindow(g_hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				// If wpPrev is successfully filled with current window placement and
				// if mi is successfully filled with primary monitor info then
				// S1: Remove WS_OVERLAPPEDWINDOW style
				// S2: Set window position by aligning left-top point's XY coordinates to
				// monitors left-top coordinates and setting window width and height to
				// monitors width and height (effectively making window full screen)
				// SWP_NOZORDER : Don't change Z-order
				// SWP_FRAMESCHANGED : Forces recalculation of Non-client area

				SetWindowLong(g_hwnd, GWL_STYLE, g_dwStyle & ~WS_OVERLAPPEDWINDOW); // S1
				SetWindowPos(g_hwnd, //S2
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		// Screen is full screen, so toggle to previously saved dimension
		// Sl: Add WS_OVERLAPPEDWINDOW to window style via SetWindowLong
		// S2: Set window placement to stored previous placement in wpPrev via SetWindowPlacement
		// S3: Force the effects of SetWindowPlacement by call to SetWindowPos with
		// SWP_NOMOVE : Don't change left top point (i.e. ignore third and fourth parameter)
		// SWP_NOSIZE : Don't change dimensions of window (i.e. ignore fifth and sixth parameter)
		// SWP_NOZORDER : Don't change Z-order of the window and it's children
		// SWP_NOOWNERZORDER : Don't change 2-order of owner of the window (refered by g_hwnd)
		// SWP_FRAMFSCHANGED : Force recaulculation of Non-Client area.
		// S4: Make cursor visible
		SetWindowLong(g_hwnd, GWL_STYLE, g_dwStyle | WS_OVERLAPPEDWINDOW); // S1
		SetWindowPlacement(g_hwnd, &g_wpPrev); // S2
		SetWindowPos(g_hwnd, // S3
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE); // S4
	}
}

void initialize(void) {
	//Prototypes of functions called from initialize

	void resize(int, int); // Handler to window resize event
	void uninitialize(void); //Release and destroy resources
	void LoadMeshData(void); //Load data from mesh file and populate the global vectors

	//CODE
	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "w");

	if (!g_fp_logfile) {
		uninitialize();
	}

	PIXELFORMATDESCRIPTOR pfd; // Information structure describing the pixel format
	int iPixelFormatIndex = 0; // Index to Pixel format structure in the internal tables

	//Fill pfd by zeros	
	ZeroMemory((void *)&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialize PIXELFORMATDESCRIPTOR with desired values of it's attribute
	// pfd.nSize : Allows Microsoft to maintain multiple versions of structure
	// pfd.nVersion : Version information
	// pfd.dwFlags
	// PFD_DRAW_TO_WINDOW : Indicates real time rendering
	// PFD_SUPPORT_OPENGL : Pixel format having this flag set will be considered for matching
	// PFD_DOUBLEBUFFER : Pixel format having this flag set will be considered for matching
	// pfd.iPixelType : Type of pixel format to be chosen
	// pfd.cColorBit : Specifies color depth in bits
	// pfd.cRedBits : Specifies bit-depth for red color
	// pfd.cGreenBits : Specifies bit-depth for green color
	// pfd.cBlueBits : Specifies bit-depth for blue color
	// pfd.cAlphaBits : Specifies bit depth for alpha component
	// pfd.cDepthBits : Specifies depth bits


	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	// Get a device context
	g_hdc = GetDC(g_hwnd);

	// Attempt to match an approximate pixel format supported by device
	// to a given pixel format specfication (via &pfd) If fails then release
	// the device context obtained above and make handle null
	iPixelFormatIndex = ChoosePixelFormat(g_hdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(g_hwnd, g_hdc);
		g_hdc = NULL;
	}

	// Sets pixel format descriptor of the specified device context (g_hdc)
	// to device supported pixel format specified by handle iPixelFormatIndex
	// Address of logical pixel format descriptor is third parameter
	// If fails release the device context and make handle NULL
	if (SetPixelFormat(g_hdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(g_hwnd, g_hdc);
		g_hdc = NULL;
	}

	// Creates an OpenGL rendering context out of specified device context
	// If fails release the device context and make handle NULL
	g_hrc = wglCreateContext(g_hdc);
	if (g_hrc == NULL)
	{
		ReleaseDC(g_hwnd, g_hdc);
		g_hdc = NULL;
	}

	// Makes specified OpenGL rendering context by g_hrc the current rendering context
	// of calling thread. If fails Delete OpenGL rendering context, release the device context
	// and make both handles NULL
	if (wglMakeCurrent(g_hdc, g_hrc) == FALSE)
	{
		ReleaseDC(g_hwnd, g_hdc);
		g_hdc = NULL;
		wglDeleteContext(g_hrc);
		g_hrc = NULL;
	}

	// Specifies clear value for the color buffer. (0.0f, 0.0f, 0.0f, 0.0f) => Black
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Add Depth :
	// S1: Enable Depth test in the state machine
	// S2: Specify the depth comparison function. GL_LEQUAL : passes if incoming z value is less than or equal to stored z value
	// S3: Specifies the clear value for depth buffer
	// S4: Select shade model, FLAT or SMOOTH
	// S5: Specify the implementation specific hint
	// GL_PERSPECTIVE_CORRECTION_HINT : Indicates quality of color and texture coordinates interpolation
	// GL_NICEST : The most correct, or the highest quality.
	
	glEnable(GL_DEPTH_TEST); //S1 
	glDepthFunc(GL_LEQUAL); //S2
	glClearDepth(1.0f); //S3
	//glShadeModel(GL_SMOOTH); //S4
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); //S5	

	// Read mesh file and load global vectors with appropriate data
	LoadMeshData();

	// Lighting parameters
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glEnable(GL_LIGHT0);

	// Warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	// If current heigh measures 0 then force it to 1 in order to avoid divide-by-zero error
	if (height == 0)
		height = 1;

	// Set the viewport transformation.
	// AP1, AP2 : (x,y) for lower left corner of viewport rectangle
	// AP3, AP4 : Width-Height of viewport rectangle
	glViewport(VIEWPORT_BOTTOMLEFT_X, VIEWPORT_BOOTOMLEFT_Y, (GLsizei)width, (GLsizei)height);

	// Set the projection transformation
	// S1 : Select projection matrix mode
	// S2 : Load identity matrix on projection matrix stack
	// S3 : Set perspective projection using gluPerspective
	//	AP1 : Field of view angle, in y-direction, in degrees
	//	AP2 : Aspect ratio
	//	AP3-AP4 : Distance from view to near clipping plane(AP3) & tofar clipping plane(AP4)
	glMatrixMode(GL_PROJECTION); // S1
	glLoadIdentity(); // S2
	gluPerspective(FOY_ANGLE, ((GLfloat)width / (GLfloat)height), ZNEAR, ZFAR); // S3
}

void LoadMeshData(void) {
	void uninitialize(void);

	// Open mesh file, name of mesh file can be parametrized
	g_fp_meshfile = fopen("MonkeyHead.OBJ", "r");

	if (!g_fp_meshfile) {
		uninitialize();
	}

	// Seperator strings
	// String holding seperator for strtok
	char *sep_space = " ";
	// String holding forward slash seperator for strtok
	char * sep_fslash = "/";

	// Token pointers
	// Character pointer for holding first word in a line
	char* first_token = NULL;
	//Character pointer for holding next word seperated by specified seperator to strtok
	char* token = NULL;

	// Array of character pointers to hold strings of face entries
	// Face entries can be variable. In some files they are three and in some they are four
	char* face_tokens[NR_FACE_TOKENS];
	// Number of non-null  tokens in above vector
	int nr_tokens;

	// Character pointer for holding string associated with vertex index
	char *token_vertex_index = NULL;

	// Character pointer for holding string associated with texture index
	char *token_texture_index = NULL;

	// Character pointer for holding string associated with normal index
	char *token_normal_index = NULL;

	// While there is line in a file 
	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL) {
		// Bind line to seperator and get first token
		first_token = strtok(line, sep_space);

		// If first token indicates vertex data
		if (strcmp(first_token, "v") == S_EQUAL) {
			//Create a vector of NR_POINT_COORDS number of floats to hold coordinates
			std::vector<float> vec_point_coord(NR_POINT_COORDS);
			
			//	Do following NR POINT COORDS time
			//	S1. Get next token
			//	S2. Feed it to atof to get floating point number out of it
			//	S3. Add the floating point number generated to vector
			//	End of loop
			//	54. At the end of loop vector is constructed, add it to
			//	global vector of vector of floats, named g_vertices 
			for (int i = 0; i != NR_POINT_COORDS; i++) {
				vec_point_coord[i] = atof(strtok(NULL, sep_space)); // S1 S2 S3
			}
			g_vertices.push_back(vec_point_coord); // S4
		}

		// If first token indicates texture data
		else if (strcmp(first_token, "vt") == S_EQUAL) {
			//Create a vector of NR_POINT_COORDS number of floats to hold coordinates
			std::vector<float> vec_texture_coord(NR_POINT_COORDS);

			//	Do following NR POINT COORDS time
			//	S1. Get next token
			//	S2. Feed it to atof to get floating point number out of it
			//	S3. Add the floating point number generated to vector
			//	End of loop
			//	54. At the end of loop vector is constructed, add it to
			//	global vector of vector of floats, named g_vertices
			for (int i = 0; i != NR_TEXTURE_COORDS; i++) {
				vec_texture_coord[i] = atof(strtok(NULL, sep_space)); // S1 S2 S3
			}
			g_vertices.push_back(vec_texture_coord); // S4
		}

		// If first token indicates normal data
		else if (strcmp(first_token, "vn") == S_EQUAL) {
			//Create a vector of NR_POINT_COORDS number of floats to hold coordinates
			std::vector<float> vec_normal_coord(NR_POINT_COORDS);

			//	Do following NR POINT COORDS time
			//	S1. Get next token
			//	S2. Feed it to atof to get floating point number out of it
			//	S3. Add the floating point number generated to vector
			//	End of loop
			//	54. At the end of loop vector is constructed, add it to
			//	global vector of vector of floats, named g_vertices
			for (int i = 0; i != NR_NORMAL_COORDS; i++) {
				vec_normal_coord[i] = atof(strtok(NULL, sep_space)); // S1 S2 S3
			}
			g_vertices.push_back(vec_normal_coord); // S4
		}

		// If first token indicates face data
		else if (strcmp(first_token, "f") == S_EQUAL) {
			// Define three vectors of int with length 3 to hold indices of triangle's 			
			// positional coordinates, texture coordinates and normal coordinates in
			// g_vertices, g_textures and g_normals respectively.
			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3),
				normal_vertex_indices(3);

			// Initialize all  char pointer in face_tokens to NULL
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);

			//Extract three fields of information in face tokens and increment
			//nr_tokens accordingly
			nr_tokens = 0;
			while (token = strtok(NULL, sep_space)) {
				if (strlen(token) < 3) {
					break;
				}
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			// Every face data entry is going to have minimum three fields therefore,
			// construct a triangle out of it with 
			// S1 : triangle coordinate data and
			// S2 : texture coodinate index data
			// S3 : normal coordinate index data
			// S4 : Put that data in triangle_vertex_indices, texture_vertex_indices,
			// normal_vertex_indices. Vectors will be constructed at the end of the loop
			for (int i = 0; i != NR_FACE_TOKENS; i++) {
				token_vertex_index = strtok(face_tokens[i], sep_fslash); // S1
				token_texture_index = strtok(NULL, sep_fslash); // S2
				token_normal_index = strtok(NULL, sep_fslash); // S3
				triangle_vertex_indices[i] = atoi(token_vertex_index); // S4 - 1
				texture_vertex_indices[i] = atoi(token_texture_index); // S4 - 2
				normal_vertex_indices[i] = atoi(token_normal_index); // S4 - 3
			}

			// Add constructed vectors to global face vectors
			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		
		// Initialize line buffer to NULL
		memset((void*)line, (int)'\0', BUFFER_SIZE);
	}

	// Close meshfile and make file pointer NULL
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	// Log vertext, texture and face data in log file
	fprintf(g_fp_logfile, "g_vertices:%llu g_texture:%llu g_normals:%llu g_face_tri:%llu\n",
		g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());
}

void update(void) {
	// Increment monkeyhead rotation angle by MONKEYHEAD_ANGLE_INCREMENT
	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;

	//If rotation angle equals or exceeds END_ANGLE_POS then reset to START_ANGLE_POS
	if (g_rotate >= END_ANGLE_POS) {
		g_rotate = START_ANGLE_POS;
	}
}

void display(void) {
	void uninitialize(void); //Release and delete resources

	// Clear color and depth buffers by their clear values
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Slect MODELVIEW matrix stack and load identity matrix in it
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Model transformation
	// S1 : Translate the monkeyhead in X, Y and Z directions respectively by 
	//		MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE
	// S2 : Rotate the monkeyhead around Y-axis by angle maintained in g_rotate
	// S3 : Perform scaling transformation along X, Y, and Z axis repectively 
	//		by MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR
	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);

	// Keep counter-clockwise winding of vertices geometry
	glFrontFace(GL_CCW);

	// Set polygon mode mentioning front and back faces and GL_LINE
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// S1 : For every face index maintained in triangular form in g_face_tri
	// Do following:
	// S2 : Set geometry primitive to GL_TRIANGLES
	// S3 : Extract triangle from outer loop index
	// for every point of a triangle
	//     S4 : Calculate the index in g_vertices in variabe vi
	//	   S5 : Calculate x, y, z coordinates of point
	//	   S6 : Send to glVertex3f()
	// Remark : In S4 we have to subtract g_face_tri[i][j] by one because the indexing in mesh file starts with 1
	//			whereas in case of array/vectors it starts with 0

	for (int i = 0; i != g_face_tri.size(); ++i) { // S1
		glBegin(GL_TRIANGLES); // S2
		for (int j = 0; j != g_face_tri[i].size(); j++) { // S3
			int vi = g_face_tri[i][j] - 1; // S4
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]); // S5, S6
			}
		glEnd();
	}

	// Bring background frame buffer to foreground make current foreground buffer available for rendering
	SwapBuffers(g_hdc);
}

void uninitialize(void) {
	// Refer to ToggleScreen() for documentation
	if (g_bFullScreen == true) {
		SetWindowLong(g_hwnd, GWL_STYLE, g_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(g_hwnd, &g_wpPrev);
		SetWindowPos(g_hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// HGLRC : NULL means calling threa's current renderning context is no longer current
	// as well as it releases the device context used by that rendering context.
	// HDC : is ignored if HGLRC is passed as NULL. Hence, HDC is also NULL
	wglMakeCurrent((HDC)NULL, (HGLRC)NULL);

	// Delete specified OpenGL rendering context. Make handle NULL.
	wglDeleteContext(g_hrc);
	g_hrc = (HGLRC)NULL;

	// Release the device context obtained via GetDC() in initialize
	ReleaseDC(g_hwnd, g_hdc);
	g_hdc = (HDC)NULL;

	// Close log file and make file pointer NULL
	fclose(g_fp_logfile);
	g_fp_logfile = NULL;

	// Sends WM_DESTROY message to specified window, marking the end of game loop
	DestroyWindow(g_hwnd);
}