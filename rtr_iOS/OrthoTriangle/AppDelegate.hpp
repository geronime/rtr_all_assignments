//
//  AppDelegate.h
//  OrthoTriangle
//
//  Created by Abhishek Jadhav on 15/06/18.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

