#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"

#import "Sphere.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat lightAmbient1[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse1[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightSpecular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition1[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat lightAmbient2[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse2[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightSpecular2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition2[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

int  modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
int  La_uniform, Ld_uniform, Ls_uniform, light_position_uniform;
int  La_uniform1, Ld_uniform1, Ls_uniform1, light_position_uniform1;
int  La_uniform2, Ld_uniform2, Ls_uniform2, light_position_uniform2;
int  Ka_uniform, Kd_uniform, Ks_uniform, material_shininess_uniform;

GLfloat sphere_vertices[]={1146};
GLfloat sphere_normals[]={1146};
GLfloat sphere_textures[]={764};
short sphere_elements[]={2280};

Sphere sphere;

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;

    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    	
    GLuint vao_Sphere;
	GLuint vbo_Sphere_Position;
	GLuint vbo_Sphere_Normal;
	GLuint vbo_sphere_element;
    
    GLuint La_uniform_left;
	GLuint Ld_uniform_left;
	GLuint Ls_uniform_left;
	GLuint left_light_position_uniform;

	GLuint La_uniform_right;
	GLuint Ld_uniform_right;
	GLuint Ls_uniform_right;
	GLuint right_light_position_uniform;

	GLuint Ka_uniform;
	GLuint Kd_uniform;
	GLuint Ks_uniform;
	GLuint material_shininess_uniform;

	GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    
    vmath::mat4 projectionMatrix;
	
	bool gbLight;
	
	int singleTap; // for toggling per-vertex and per-fragment shading
    int doubleTap; // for lights
	float gLightAngleY = 0.0f;
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        // *** VERTEX SHADER ***
        // create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n"+
		"precision highp int;"+
		"uniform int u_perVertex, u_perFragment;"+
		"precision highp float;"+
        "in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+		
		"uniform int u_LKeyPressed;"+
		"uniform vec3 u_La, u_Ld, u_Ls;"+
		"uniform vec3 u_La1, u_Ld1, u_Ls1;"+
		"uniform vec3 u_La2, u_Ld2, u_Ls2;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform vec4 u_light_position, u_light_position1, u_light_position2;"+
		"uniform float u_material_shininess;"+
		"out vec3 out_phong_ads_color;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_direction, light_direction1, light_direction2;"+
		"out vec3 viewer_vector;"+
		 
		"uniform int u_double_tap;"+
		 
		 "vec3 calculateLight(vec3 u_La, vec3 u_Ld, vec3 u_Ls, vec4 u_light_position)"+
		"{"+
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"+
			"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
			"vec3 light_direction = normalize(vec3(u_light_position) - eyeCoordinates.xyz);"+
			"float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);"+
			"vec3 ambient = u_La * u_Kd;"+
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
			"vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
			"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);"+
			"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
			"out_phong_ads_color = ambient + diffuse + specular;"+
			"return out_phong_ads_color;"+
		"}"+
		 
		"void main(void)"+
		"{"+
		"if (u_double_tap == 1)"+
		"{" +
			"if(u_perVertex == 1)"+
			"{"+
				"vec3 light0 = calculateLight(u_La, u_Ld, u_Ls, u_light_position);"+
				"vec3 light1 = calculateLight(u_La1, u_Ld1, u_Ls1, u_light_position1);"+
				"vec3 light2 = calculateLight(u_La2, u_Ld2, u_Ls2, u_light_position2);"+
				"out_phong_ads_color += light0 + light1 + light2;"+
			"}"+

			"else if(u_perFragment == 1)"+
			"{"+
				"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
				"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
				"light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
				"light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;"+
				"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;"+
			"}"+

			"else"+
			"{"+
				"out_phong_ads_color = vec3(1.0, 1.0, 1.0);"+
			"}"+
		"}"+
		"else"+
		"{"+
			"out_phong_ads_color = vec3(1.0, 1.0, 1.0);"+
		"}"+
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}"
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n"+
		"precision highp int;"+
		"uniform int u_perVertex;"+
		"uniform int u_perFragment;"+
        "precision highp float;"+		 
        "in vec3 transformed_normals;"+
		"in vec3 light_direction, light_direction1, light_direction2;"+
		"in vec3 viewer_vector;"+
		"in vec3 out_phong_ads_color;"+
		"out vec4 FragColor;"+
		"uniform vec3 u_La, u_Ld, u_Ls;"+
		"uniform vec3 u_La1, u_Ld1, u_Ls1;"+
		"uniform vec3 u_La2, u_Ld2, u_Ls2;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"uniform int u_double_tap;" +		
		
		"vec3 calculateLight(vec3 u_La, vec3 u_Ld, vec3 u_Ls, vec3 light_direction)"+
		"{"+
			"vec3 phong_ads_color;"+
			"vec3 normalized_transformed_normals=normalize(transformed_normals);"+
			"vec3 normalized_light_direction=normalize(light_direction);"+
			"vec3 normalized_viewer_vector=normalize(viewer_vector);"+
			"vec3 ambient = u_La * u_Ka;"+
			"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
			"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
			"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
			"phong_ads_color=ambient + diffuse + specular;"+
			"return phong_ads_color;"+
		"}"+
		
		"void main(void)"+
		"{"+
			"vec3 phong_ads_color;"+
			"if (u_double_tap == 1)"+
			"{"+
				"if(u_perVertex == 1)"+
				"{"+
					"FragColor = vec4(out_phong_ads_color, 1.0);"+
				"}"+

				"else if(u_perFragment == 1)"+
				"{"+
					"vec3 light0 = calculateLight(u_La, u_Ld, u_Ls, light_direction);"+
					"vec3 light1 = calculateLight(u_La1, u_Ld1, u_Ls1, light_direction1);"+
					"vec3 light2 = calculateLight(u_La2, u_Ld2, u_Ls2, light_direction2);"+
					"phong_ads_color += light0 + light1 + light2;"+
					"FragColor = vec4(phong_ads_color, 1.0f);"+
				"}"+

				"else"+
				"{"+
					"FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);"+
				"}"+
			"}"+

			"else"+
			"{"+
				"FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);"+
			"}"+
		"}"
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

        // link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
           model_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");

	// Ambience color intensity of the light
	La_uniform =glGetUniformLocation(shaderProgramObject, "u_La");
	// Diffuse color intensity of the light
	Ld_uniform =glGetUniformLocation(shaderProgramObject, "u_Ld");
	// Specular color intensity of light
	Ls_uniform =glGetUniformLocation(shaderProgramObject, "u_Ls");
	// Light position
	light_position_uniform =glGetUniformLocation(shaderProgramObject, "u_light_position");		

	// Ambience color intensity of the light
	La_uniform1 =glGetUniformLocation(shaderProgramObject, "u_La1");
	// Diffuse color intensity of the light
	Ld_uniform1 =glGetUniformLocation(shaderProgramObject, "u_Ld1");
	// Specular color intensity of light
	Ls_uniform1 =glGetUniformLocation(shaderProgramObject, "u_Ls1");
	// Light position
	light_position_uniform1 =glGetUniformLocation(shaderProgramObject, "u_light_position1");

	// Ambience color intensity of the light
	La_uniform2 =glGetUniformLocation(shaderProgramObject, "u_La2");
	// Diffuse color intensity of the light
	Ld_uniform2 =glGetUniformLocation(shaderProgramObject, "u_Ld2");
	// Specular color intensity of light
	Ls_uniform2 =glGetUniformLocation(shaderProgramObject, "u_Ls2");
	// Light position
	light_position_uniform2 =glGetUniformLocation(shaderProgramObject, "u_light_position2");

	Ka_uniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
	Kd_uniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
	Ks_uniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
    
	[sphere getSphereVertexData:sphere_vertices :sphere_normals :sphere_textures :sphere_elements];	
	numVertices = [sphere getNumberOfSphereVertices];
	numElements = [sphere getNumberOfSphereElements];
	
        
    // Vertex Array Object
	glGenVertexArrays(1, &vao_Sphere);
	glBindVertexArray(vao_Sphere);

	// Vertex Buffer Object
	/*****Vertex Buffer Object Position Start******/
	glGenBuffers(1, &vbo_Sphere_Position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Sphere_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(PND_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(PND_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/*****Vertex Buffer Object Position End******/

	/*****Vertex Buffer Object Normal Start******/
	glGenBuffers(1, &vbo_Sphere_Normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Sphere_Normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(PND_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(PND_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/*****Vertex Buffer Object Normal End******/

	/*****Vertex Buffer Object for Element Start******/
	glGenBuffers(1, &vbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	/*****Vertex Buffer Object for Element End******/

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // black
        
        // set projection matrix to identity matrix
        projectionMatrix = vmath::mat4::identity();
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath=[[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if (!bmpImage)
    {
        NSLog(@"can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage=bmpImage.CGImage;
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4, for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    // Create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTexture);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    // code
    float lightYRotateZAxis = (float) (Math.cos(3.1415 * gLightAngleY / 180.0) * 10.0);
	float lightYRotateXAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);

	float lightXRotateZAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);
	float lightXRotateYAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);

	float lightZRotateYAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);
	float lightZRotateXAxis = (float) (Math.sin(3.1415 * gLightAngleY / 180.0) * 10.0);
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using OpenGL program object
    glUseProgram(shaderProgramObject);
    
    /** Here goes the code for OpenGL drawing**/
    
    /**************Triangle drawing start**************/
    
    // Set ModelView and ModelViewProjection matrices to identity
    if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);
		
		lightPosition[0] = 0.0f;
		lightPosition[1] = lightXRotateYAxis;
		lightPosition[2] = lightXRotateZAxis;
		lightPosition[3] = 1.0f;

		lightPosition1[0] = lightYRotateXAxis;
		lightPosition1[1] = 0.0f;
		lightPosition1[2] = lightYRotateZAxis;
		lightPosition1[3] = 1.0f;

		lightPosition2[0] = lightZRotateXAxis;
		lightPosition2[1] = lightZRotateYAxis;
		lightPosition2[2] = 0.0f;
		lightPosition2[3] = 1.0f;
		
		// Set light
		glUniform3fv(La_uniform, 1, lightAmbient, 0);
		glUniform3fv(Ld_uniform, 1, lightDiffuse, 0);
		glUniform3fv(Ls_uniform, 1, lightSpecular, 0);
		glUniform4fv(light_position_uniform, 1, lightPosition, 0);

		glUniform3fv(La_uniform1, 1, lightAmbient1, 0);
		glUniform3fv(Ld_uniform1, 1, lightDiffuse1, 0);
		glUniform3fv(Ls_uniform1, 1, lightSpecular1, 0);
		glUniform4fv(light_position_uniform1, 1, lightPosition1, 0);

		glUniform3fv(La_uniform2, 1, lightAmbient2, 0);
		glUniform3fv(Ld_uniform2, 1, lightDiffuse2, 0);
		glUniform3fv(Ls_uniform2, 1, lightSpecular2, 0);
		glUniform4fv(light_position_uniform2, 1, lightPosition2, 0);

		// Set material properties
		glUniform3fv(Ka_uniform, 1, material_ambient, 0);
		glUniform3fv(Kd_uniform, 1, material_diffuse, 0);
		glUniform3fv(Ks_uniform, 1, material_specular, 0);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	// Translate the triangle to make it visible
	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	// Bind vao
	glBindVertexArray(vao_Sphere);

	// Draw the sphere
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
	glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);

	// Unbind vao
	glBindVertexArray(0);

	// Stop using the shaderProgramObject
	glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
        
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    //glOrtho(left,right,bottom,top,near,far);
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    // perspective projection
    projectionMatrix=vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{

}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{

}

- (void)dealloc
{
    // code
    if(vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid=0;
    }
    
    // *** destroy vbos ***
    if(vbo_pyramid_position)
    {
        glDeleteBuffers(1, &vbo_pyramid_position);
        vbo_pyramid_position=0;
    }
    
    if(vbo_pyramid_texture)
    {
        glDeleteBuffers(1, &vbo_pyramid_texture);
        vbo_pyramid_texture=0;
    }
    
    // destroy texture
    if(pyramid_texture)
    {
        glDeleteTextures(1,&pyramid_texture);
        pyramid_texture=0;
    }
    
    if(vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube=0;
    }
    
    // *** destroy vbos ***
    if(vbo_cube_position)
    {
        glDeleteBuffers(1, &vbo_cube_position);
        vbo_cube_position=0;
    }
    
    if(vbo_cube_texture)
    {
        glDeleteBuffers(1, &vbo_cube_texture);
        vbo_cube_texture=0;
    }
    
    // destroy texture
    if(cube_texture)
    {
        glDeleteTextures(1,&cube_texture);
        cube_texture=0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
