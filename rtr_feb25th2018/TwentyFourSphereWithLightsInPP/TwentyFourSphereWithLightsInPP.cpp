#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/GL.h>

#include "vmath.h"
#include"Sphere.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 800

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")

using namespace vmath;

enum
{
	PND_ATTRIBUTE_VERTEX = 0,
	PND_ATTRIBUTE_COLOR,
	PND_ATTRIBUTE_NORMAL,
	PND_ATTRIBUTE_TEXTURE0,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
bool bDone = false;
HDC ghdc = NULL;
HGLRC hglrc = NULL;
bool gbActiveWindow = false, gbEscapeKeyIsPressed = false, gbFullscreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(wpPrev) };

FILE *gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;
GLuint gMVPUniform;

GLuint gNumVertices;
GLuint gNumElements;

GLfloat sin_red = 0.0f;
GLfloat cos_red = 0.0f;

GLfloat sin_green = 0.0f;
GLfloat cos_green = 0.0f;

GLfloat sin_blue = 0.0f;
GLfloat cos_blue = 0.0f;


float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLfloat rotationAngle = 0.0f;

GLuint gViewMatrixUniform, gModelMatrixUniform, gProjectionMatrixUniform, view_matrix_uniform;
GLuint gLdUniform, gKdUniform, glightPosition_RedUniform;

GLuint gLKeyPressedUniform;

GLuint L_KeyPressed_uniform;
GLuint F_KeyPressed_uniform;
GLuint V_KeyPressed_uniform;

GLuint La_uniform_red;
GLuint Ld_uniform_red;
GLuint Ls_uniform_red;
GLuint light_position_uniform_red;

GLuint La_uniform_green;
GLuint Ld_uniform_green;
GLuint Ls_uniform_green;
GLuint light_position_uniform_green;

GLuint La_uniform_blue;
GLuint Ld_uniform_blue;
GLuint Ls_uniform_blue;
GLuint light_position_uniform_blue;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

bool gbVertexLighting = false;
bool gbFragmentLighting = false;

GLuint gwidth = 0;
GLuint gheight = 0;

GLfloat lightAmbient_red[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse_red[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular_red[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightPosition_red[] = { -2.0f,1.0f,0.0f,1.0f };

GLfloat lightAmbient_green[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse_green[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular_green[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat lightPosition_green[] = { 2.0f,1.0f,0.0f,1.0f };

GLfloat lightAmbient_blue[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse_blue[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular_blue[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightPosition_blue[] = { 0.0f,0.0f,2.0f,1.0f };

//Material
// 1 x 1
GLfloat material_ambient_1_1[] = { 0.0215f,0.1745f,0.0215f,1.0f };
GLfloat material_diffuse_1_1[] = { 0.07568f,0.61424f,0.07568f,1.0f };
GLfloat material_specular_1_1[] = { 0.633f,0.727811f,0.633f,1.0f };
GLfloat material_shininess_1_1 = { 0.6f * 128.0f };

// 1 x 2
GLfloat material_ambient_1_2[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_diffuse_1_2[] = { 0.54f,0.89f,0.63f,1.0f };
GLfloat material_specular_1_2[] = { 0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_shininess_1_2 = { 0.1f * 128.0f };

// 1 x 3
GLfloat material_ambient_1_3[] = { 0.05357f,0.05f,0.06625f,1.0f };
GLfloat material_diffuse_1_3[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_specular_1_3[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_shininess_1_3 = { 0.3f * 128.0f };

// 1 x 4
GLfloat material_ambient_1_4[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_diffuse_1_4[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_specular_1_4[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_shininess_1_4 = { 0.088f * 128.0f };

// 1 x 5
GLfloat material_ambient_1_5[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_diffuse_1_5[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_specular_1_5[] = { 0.727811f,0.626959f,0.626959f,1.0f };
GLfloat material_shininess_1_5 = { 0.6f * 128.0f };

// 1 x 6
GLfloat material_ambient_1_6[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_diffuse_1_6[] = { 0.369f,0.74151f,0.69102f,1.0f };
GLfloat material_specular_1_6[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_shininess_1_6 = { 0.1f * 128.0f };

// 2 x 1
GLfloat material_ambient_2_1[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_diffuse_2_1[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_specular_2_1[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_shininess_2_1 = { 0.21794872f * 128.0f };

// 2 x 2
GLfloat material_ambient_2_2[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_diffuse_2_2[] = { 0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_specular_2_2[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_shininess_2_2 = { 0.2f * 128.0f };

// 2 x 3
GLfloat material_ambient_2_3[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat material_diffuse_2_3[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_specular_2_3[] = { 0.774597f,0.774597f,0.774597f,1.0f };
GLfloat material_shininess_2_3 = { 0.6f * 128.0f };

// 2 x 4
GLfloat material_ambient_2_4[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_diffuse_2_4[] = { 0.7038f,0.27048f,0.0828f,1.0f };
GLfloat material_specular_2_4[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_shininess_2_4 = { 0.1f * 128.0f };

// 2 x 5
GLfloat material_ambient_2_5[] = { 0.24725f,0.1995f,0.0745f,1.0f };
GLfloat material_diffuse_2_5[] = { 0.75164f,0.60648f,0.22648f,1.0f };
GLfloat material_specular_2_5[] = { 0.628281f,0.555802f,0.366065f,1.0f };
GLfloat material_shininess_2_5 = { 0.4f * 128.0f };

// 2 x 6
GLfloat material_ambient_2_6[] = { 0.19255f,0.19225f,0.19225f,1.0f };
GLfloat material_diffuse_2_6[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_specular_2_6[] = { 0.58273f,0.508273f,0.508273f,1.0f };
GLfloat material_shininess_2_6 = { 0.4f * 128.0f };

// 3 x 1
GLfloat material_ambient_3_1[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse_3_1[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_specular_3_1[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_shininess_3_1 = { 0.25f * 128.0f };

// 3 x 2
GLfloat material_ambient_3_2[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_diffuse_3_2[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_specular_3_2[] = { 0.50196078f,0.50196078f,0.50196078f,1.0f };
GLfloat material_shininess_3_2 = { 0.25f * 128.0f };

// 3 x 3
GLfloat material_ambient_3_3[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse_3_3[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_specular_3_3[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_shininess_3_3 = { 0.25f * 128.0f };

// 3 x 4
GLfloat material_ambient_3_4[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse_3_4[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_specular_3_4[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_shininess_3_4 = { 0.25f * 128.0f };

// 3 x 5
GLfloat material_ambient_3_5[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse_3_5[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_specular_3_5[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_shininess_3_5 = { 0.25f * 128.0f };

// 3 x 6
GLfloat material_ambient_3_6[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse_3_6[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_specular_3_6[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_shininess_3_6 = { 0.25f * 128.0f };

// 4 x 1
GLfloat material_ambient_4_1[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_diffuse_4_1[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_specular_4_1[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_shininess_4_1 = { 0.078125f * 128.0f };

// 4 x 2
GLfloat material_ambient_4_2[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_diffuse_4_2[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_specular_4_2[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_shininess_4_2 = { 0.78125f * 128.0f };

// 4 x 3
GLfloat material_ambient_4_3[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_diffuse_4_3[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_specular_4_3[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_shininess_4_3 = { 0.078125f * 128.0f };

// 4 x 4
GLfloat material_ambient_4_4[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_diffuse_4_4[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_specular_4_4[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_shininess_4_4 = { 0.078125f * 128.0f };

// 4 x 5
GLfloat material_ambient_4_5[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_diffuse_4_5[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_specular_4_5[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_shininess_4_5 = { 0.078125f * 128.0f };

// 4 x 6
GLfloat material_ambient_4_6[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_diffuse_4_6[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_specular_4_6[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_shininess_4_6 = { 0.078125f * 128.0f };


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	void display(void);
	void spin(void);
	void initialize(void);
	void uninitialize(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szWndClsName[] = TEXT("PP-SpherePerVertex-PerFragment");
	int screenWidth = 0;
	int screenHeight = 0;
	RECT rc;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	if (!SystemParametersInfo(SPI_GETWORKAREA, 0, &rc, 0)) {
		MessageBox(NULL, TEXT("Failed to get screen dimension"), TEXT("Error: SystemParameterInfo"), MB_OK);
	}

	screenWidth = rc.right - rc.left;
	screenHeight = rc.bottom - rc.top;


	wndClass.cbSize = sizeof(wndClass);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpszClassName = szWndClsName;
	wndClass.lpszMenuName = NULL;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	if (!RegisterClassEx(&wndClass)) {
		MessageBox(NULL, TEXT("Failed to register class"), TEXT("Error: RegisterClassEx"), MB_OK);
		exit(0);
	}

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szWndClsName, TEXT("PP-SpherePerVertex-PerFragment"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, ((screenWidth / 2) - (WIN_WIDTH / 2)), ((screenHeight / 2) - (WIN_HEIGHT / 2)), WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	if (hwnd == NULL) {
		MessageBox(NULL, TEXT("Failed to create window"), TEXT("Error: CreateWindowEx"), MB_OK);
		exit(0);
	}


	ghwnd = hwnd;

	initialize();

	SetForegroundWindow(hwnd);
	ShowWindow(hwnd, SW_SHOW);
	SetFocus(hwnd);

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			// Render
			if (gbActiveWindow == true) {
				if (gbEscapeKeyIsPressed == true) {
					bDone = true;
				}
				spin();
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	void resize(int, int);
	void MakeFullscreen(void);
	void LeaveFullscreen(void);

	static bool bIsLKeyPressed = false;
	static bool bIsVKeyPressed = false;
	static bool bIsFKeyPressed = false;

	switch (iMsg) {
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam) {
		case 0x51: //For Q
			gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // F
			gbVertexLighting = false;
			gbFragmentLighting = true;
			break;

		case 0x1B: // For Escape
			if (!gbFullscreen) {
				MakeFullscreen();
				gbFullscreen = true;
			}
			else {
				LeaveFullscreen();
				gbFullscreen = false;
			}
			break;

		case 0x56: // V
			gbVertexLighting = true;
			gbFragmentLighting = false;
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void initialize(void) {
	void resize(int, int);
	void uninitialize(void);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	ZeroMemory(&pfd, sizeof(pfd));

	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		MessageBox(ghwnd, TEXT("Failed to Choose Pixel Format"), TEXT("Error: ChoosePixelFormat"), MB_OK);
		exit(0);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		MessageBox(ghwnd, TEXT("Failed to Set pixel format"), TEXT("Error: SetPixelFormat"), MB_OK);
		DestroyWindow(ghwnd);
	}

	hglrc = wglCreateContext(ghdc);
	if (hglrc == FALSE) {
		MessageBox(ghwnd, TEXT("Failed to Create Context"), TEXT("Error: wglCreateContext()"), MB_OK);
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, hglrc) == FALSE) {
		MessageBox(ghwnd, TEXT("Failed to Make Current Context"), TEXT("Error: wglMakeCurrent"), MB_OK);
		wglDeleteContext(hglrc);
		hglrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK) {
		wglDeleteContext(hglrc);
		hglrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	// Vertex Shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;" \
		"uniform int u_f_lighting_enabled;" \
		"uniform int u_v_lighting_enabled;" \
		"uniform vec3 u_La_red;" \
		"uniform vec3 u_Ld_red;" \
		"uniform vec3 u_Ls_red;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec3 u_La_green;" \
		"uniform vec3 u_Ld_green;" \
		"uniform vec3 u_Ls_green;" \
		"uniform vec4 u_light_position_green;" \
		"uniform vec3 u_La_blue;" \
		"uniform vec3 u_Ld_blue;" \
		"uniform vec3 u_Ls_blue;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"vec3 phong_ads_color;" \
		"out vec3 out_phong_ads_color;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction_red;" \
		"out vec3 light_direction_green;" \
		"out vec3 light_direction_blue;" \
		"out vec3 viewer_vector;" \
		"vec3 CalculateLight(vec3 La, vec3 Ld, vec3 Ls, vec4 light_position)"
		"{" \
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_direction = normalize(vec3(light_position) - eyeCoordinates.xyz);" \
		"float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
		"vec3 ambient = La * u_Kd;" \
		"vec3 diffuse = Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
		"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
		"vec3 specular = Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color = ambient + diffuse + specular;" \
		"return phong_ads_color;" \
		"}" \
		"void main(void)" \
		"{" \
		"if(u_v_lighting_enabled == 1)" \
		"{" \
		"vec3 light_red = CalculateLight(u_La_red, u_Ld_red, u_Ls_red, u_light_position_red);" \
		"vec3 light_green = CalculateLight(u_La_green, u_Ld_green, u_Ls_green, u_light_position_green);" \
		"vec3 light_blue = CalculateLight(u_La_blue, u_Ld_blue, u_Ls_blue, u_light_position_blue);" \
		"out_phong_ads_color = light_red + light_green + light_blue;" \
		"}" \
		"if(u_f_lighting_enabled == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction_red = vec3(u_light_position_red) - eye_coordinates.xyz;" \
		"light_direction_green = vec3(u_light_position_green) - eye_coordinates.xyz;" \
		"light_direction_blue = vec3(u_light_position_blue) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Fragment Shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction_red;" \
		"in vec3 light_direction_green;" \
		"in vec3 light_direction_blue;" \
		"in vec3 viewer_vector;" \
		"in vec3 out_phong_ads_color;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La_red;" \
		"uniform vec3 u_Ld_red;" \
		"uniform vec3 u_Ls_red;" \
		"uniform vec3 u_La_green;" \
		"uniform vec3 u_Ld_green;" \
		"uniform vec3 u_Ls_green;" \
		"uniform vec3 u_La_blue;" \
		"uniform vec3 u_Ld_blue;" \
		"uniform vec3 u_Ls_blue;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"uniform int u_v_lighting_enabled;" \
		"uniform int u_f_lighting_enabled;" \
		"vec3 CalculateLight(vec3 u_La, vec3 u_Ld, vec3 u_Ls, vec3 light_direction)" \
		"{" \
		"vec3 phong_ads_color;" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"return phong_ads_color;" \
		"}" \
		"void main(void)" \
		"{" \
		"if(u_v_lighting_enabled == 1)" \
		"{" \
		"FragColor = vec4(out_phong_ads_color, 1.0);" \
		"}" \
		"if(u_f_lighting_enabled == 1)" \
		"{" \
		"vec3 light_red = CalculateLight(u_La_red, u_Ld_red, u_Ls_red, light_direction_red);" \
		"vec3 light_green = CalculateLight(u_La_green, u_Ld_green, u_Ls_green, light_direction_green);" \
		"vec3 light_blue = CalculateLight(u_La_blue, u_Ld_blue, u_Ls_blue, light_direction_blue);" \
		"vec3 phong_ads_color = light_red + light_green + light_blue;" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Shader Program
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, PND_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, PND_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_f_lighting_enabled");
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_v_lighting_enabled");

	La_uniform_red = glGetUniformLocation(gShaderProgramObject, "u_La_red");
	Ld_uniform_red = glGetUniformLocation(gShaderProgramObject, "u_Ld_red");
	Ls_uniform_red = glGetUniformLocation(gShaderProgramObject, "u_Ls_red");
	light_position_uniform_red = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");

	La_uniform_green = glGetUniformLocation(gShaderProgramObject, "u_La_green");
	Ld_uniform_green = glGetUniformLocation(gShaderProgramObject, "u_Ld_green");
	Ls_uniform_green = glGetUniformLocation(gShaderProgramObject, "u_Ls_green");
	light_position_uniform_green = glGetUniformLocation(gShaderProgramObject, "u_light_position_green");

	La_uniform_blue = glGetUniformLocation(gShaderProgramObject, "u_La_blue");
	Ld_uniform_blue = glGetUniformLocation(gShaderProgramObject, "u_Ld_blue");
	Ls_uniform_blue = glGetUniformLocation(gShaderProgramObject, "u_Ls_blue");
	light_position_uniform_blue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");

	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// Position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(PND_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(PND_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Normals
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(PND_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(PND_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Elements
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void MakeFullscreen(void) {
	MONITORINFO mi = { sizeof(mi) };
	dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

	if (dwStyle & WS_OVERLAPPEDWINDOW) {
		if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
		}
	}
	ShowCursor(FALSE);
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	sin_red = cos(3.1415 * rotationAngle / 180.0f) * 10.0f;
	cos_red = sin(3.1415 * rotationAngle / 180.0f) * 10.0f;

	sin_green = cos(3.1415 * rotationAngle / 180.0f) * 10.0f;
	cos_green = sin(3.1415 * rotationAngle / 180.0f) * 10.0f;

	sin_blue = cos(3.1415 * rotationAngle / 180.0f) * 10.0f;
	cos_blue = sin(3.1415 * rotationAngle / 180.0f) * 10.0f;

	glUniform1i(V_KeyPressed_uniform, 1);

	if (gbFragmentLighting == true) {
		glUniform1i(F_KeyPressed_uniform, 1);
		glUniform1i(V_KeyPressed_uniform, 0);
	}
	else {
		glUniform1i(F_KeyPressed_uniform, 0);
		glUniform1i(V_KeyPressed_uniform, 1);
	}

	glUniform3fv(La_uniform_red, 1, lightAmbient_red);
	glUniform3fv(Ld_uniform_red, 1, lightDiffuse_red);
	glUniform3fv(Ls_uniform_red, 1, lightSpecular_red);
	lightPosition_red[0] = cos_red;
	lightPosition_red[1] = 0.0f;
	lightPosition_red[2] = sin_red;
	glUniform4fv(light_position_uniform_red, 1, lightPosition_red);

	glUniform3fv(La_uniform_green, 1, lightAmbient_green);
	glUniform3fv(Ld_uniform_green, 1, lightDiffuse_green);
	glUniform3fv(Ls_uniform_green, 1, lightSpecular_green);
	lightPosition_green[0] = 0.0f;
	lightPosition_green[1] = cos_green;
	lightPosition_green[2] = sin_green;
	glUniform4fv(light_position_uniform_green, 1, lightPosition_green);

	glUniform3fv(La_uniform_blue, 1, lightAmbient_blue);
	glUniform3fv(Ld_uniform_blue, 1, lightDiffuse_blue);
	glUniform3fv(Ls_uniform_blue, 1, lightSpecular_blue);
	lightPosition_blue[0] = cos_blue;
	lightPosition_blue[1] = sin_blue;
	lightPosition_blue[2] = 0.0f;
	glUniform4fv(light_position_uniform_blue, 1, lightPosition_blue);

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	GLint currentWidth = 0;
	GLint currentHeight = 0;

	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glViewport(0, 0, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_1_1);
	glUniform3fv(Kd_uniform, 1, material_diffuse_1_1);
	glUniform3fv(Ks_uniform, 1, material_specular_1_1);
	glUniform1f(material_shininess_uniform, material_shininess_1_1);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, 0, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_1_2);
	glUniform3fv(Kd_uniform, 1, material_diffuse_1_2);
	glUniform3fv(Ks_uniform, 1, material_specular_1_2);
	glUniform1f(material_shininess_uniform, material_shininess_1_2);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, 0, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_1_3);
	glUniform3fv(Kd_uniform, 1, material_diffuse_1_3);
	glUniform3fv(Ks_uniform, 1, material_specular_1_3);
	glUniform1f(material_shininess_uniform, material_shininess_1_3);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, 0, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_1_4);
	glUniform3fv(Kd_uniform, 1, material_diffuse_1_4);
	glUniform3fv(Ks_uniform, 1, material_specular_1_4);
	glUniform1f(material_shininess_uniform, material_shininess_1_4);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, 0, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_1_5);
	glUniform3fv(Kd_uniform, 1, material_diffuse_1_5);
	glUniform3fv(Ks_uniform, 1, material_specular_1_5);
	glUniform1f(material_shininess_uniform, material_shininess_1_5);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, 0, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_1_6);
	glUniform3fv(Kd_uniform, 1, material_diffuse_1_6);
	glUniform3fv(Ks_uniform, 1, material_specular_1_6);
	glUniform1f(material_shininess_uniform, material_shininess_1_6);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	// 2nd Row
	currentHeight += gheight / 4;
	currentWidth = 0;

	glViewport(0, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_2_1);
	glUniform3fv(Kd_uniform, 1, material_diffuse_2_1);
	glUniform3fv(Ks_uniform, 1, material_specular_2_1);
	glUniform1f(material_shininess_uniform, material_shininess_2_1);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_2_2);
	glUniform3fv(Kd_uniform, 1, material_diffuse_2_2);
	glUniform3fv(Ks_uniform, 1, material_specular_2_2);
	glUniform1f(material_shininess_uniform, material_shininess_2_2);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_2_3);
	glUniform3fv(Kd_uniform, 1, material_diffuse_2_3);
	glUniform3fv(Ks_uniform, 1, material_specular_2_3);
	glUniform1f(material_shininess_uniform, material_shininess_2_3);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_2_4);
	glUniform3fv(Kd_uniform, 1, material_diffuse_2_4);
	glUniform3fv(Ks_uniform, 1, material_specular_2_4);
	glUniform1f(material_shininess_uniform, material_shininess_2_4);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_2_5);
	glUniform3fv(Kd_uniform, 1, material_diffuse_2_5);
	glUniform3fv(Ks_uniform, 1, material_specular_2_5);
	glUniform1f(material_shininess_uniform, material_shininess_2_5);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_2_6);
	glUniform3fv(Kd_uniform, 1, material_diffuse_2_6);
	glUniform3fv(Ks_uniform, 1, material_specular_2_6);
	glUniform1f(material_shininess_uniform, material_shininess_2_6);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	// 3rd Row
	currentHeight += gheight / 4;
	currentWidth = 0;

	glViewport(0, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_3_1);
	glUniform3fv(Kd_uniform, 1, material_diffuse_3_1);
	glUniform3fv(Ks_uniform, 1, material_specular_3_1);
	glUniform1f(material_shininess_uniform, material_shininess_3_1);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_3_2);
	glUniform3fv(Kd_uniform, 1, material_diffuse_3_2);
	glUniform3fv(Ks_uniform, 1, material_specular_3_2);
	glUniform1f(material_shininess_uniform, material_shininess_3_2);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_3_3);
	glUniform3fv(Kd_uniform, 1, material_diffuse_3_3);
	glUniform3fv(Ks_uniform, 1, material_specular_3_3);
	glUniform1f(material_shininess_uniform, material_shininess_3_3);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_3_4);
	glUniform3fv(Kd_uniform, 1, material_diffuse_3_4);
	glUniform3fv(Ks_uniform, 1, material_specular_3_4);
	glUniform1f(material_shininess_uniform, material_shininess_3_4);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_3_5);
	glUniform3fv(Kd_uniform, 1, material_diffuse_3_5);
	glUniform3fv(Ks_uniform, 1, material_specular_3_5);
	glUniform1f(material_shininess_uniform, material_shininess_3_5);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_3_6);
	glUniform3fv(Kd_uniform, 1, material_diffuse_3_6);
	glUniform3fv(Ks_uniform, 1, material_specular_3_6);
	glUniform1f(material_shininess_uniform, material_shininess_3_6);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	// 4th Row
	currentHeight += gheight / 4;
	currentWidth = 0;

	glViewport(0, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_4_1);
	glUniform3fv(Kd_uniform, 1, material_diffuse_4_1);
	glUniform3fv(Ks_uniform, 1, material_specular_4_1);
	glUniform1f(material_shininess_uniform, material_shininess_4_1);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_4_2);
	glUniform3fv(Kd_uniform, 1, material_diffuse_4_2);
	glUniform3fv(Ks_uniform, 1, material_specular_4_2);
	glUniform1f(material_shininess_uniform, material_shininess_4_2);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_4_3);
	glUniform3fv(Kd_uniform, 1, material_diffuse_4_3);
	glUniform3fv(Ks_uniform, 1, material_specular_4_3);
	glUniform1f(material_shininess_uniform, material_shininess_4_3);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_4_4);
	glUniform3fv(Kd_uniform, 1, material_diffuse_4_4);
	glUniform3fv(Ks_uniform, 1, material_specular_4_4);
	glUniform1f(material_shininess_uniform, material_shininess_4_4);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_4_5);
	glUniform3fv(Kd_uniform, 1, material_diffuse_4_5);
	glUniform3fv(Ks_uniform, 1, material_specular_4_5);
	glUniform1f(material_shininess_uniform, material_shininess_4_5);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	currentWidth += gwidth / 6;

	glViewport(currentWidth, currentHeight, gwidth / 6, gheight / 4);
	glUniform3fv(Ka_uniform, 1, material_ambient_4_6);
	glUniform3fv(Kd_uniform, 1, material_diffuse_4_6);
	glUniform3fv(Ks_uniform, 1, material_specular_4_6);
	glUniform1f(material_shininess_uniform, material_shininess_4_6);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

void spin(void) {
	rotationAngle += 0.1f;
	if (rotationAngle >= 360.0f)
		rotationAngle = 0.0f;
}

void resize(int width, int height) {
	if (height == 0)
		height = 1;

	gwidth = width;
	gheight = height;

	gPerspectiveProjectionMatrix = perspective(45.0f, (float)width / (float)height, 0.1f, 100.0f);
}

void LeaveFullscreen(void) {
	SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
	SetWindowPlacement(ghwnd, &wpPrev);
	SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
	ShowCursor(TRUE);
}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);

	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(hglrc);
	hglrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}