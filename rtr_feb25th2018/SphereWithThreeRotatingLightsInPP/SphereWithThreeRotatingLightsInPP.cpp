#include <windows.h>
#include <stdio.h> 

#include <gl\glew.h>
#include <gl\GL.h>

#include "vmath.h"
#include "Sphere.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum {
	PND_ATTRIBUTE_VERTEX = 0,
	PND_ATTRIBUTE_COLOR,
	PND_ATTRIBUTE_NORMAL,
	PND_ATTRIBUTE_TEXTURE0
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbQKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gpvVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Sphere;
GLuint gVbo_Sphere_Position;
GLuint gVbo_Sphere_Normal;
GLuint gVbo_sphere_element;

GLuint gModelViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLdUniform, gKdUniform, gLightPositionUniform;
GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
GLuint L_KeyPressed_uniform;
GLuint V_KeyPressed_uniform;
GLuint F_KeyPressed_uniform;

mat4 gPerspectiveProjectionMatrix;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gNumVertices;
GLuint gNumElements;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat lightAmbient1[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse1[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightSpecular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition1[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat lightAmbient2[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse2[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightSpecular2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition2[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

GLuint La_uniform1;
GLuint Ld_uniform1;
GLuint Ls_uniform1;
GLuint light_position_uniform1;

GLuint La_uniform2;
GLuint Ld_uniform2;
GLuint Ls_uniform2;
GLuint light_position_uniform2;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

GLfloat gLightAngleY = 0.0f;

bool gbLight;
bool gbPerVertex;
bool gbPerFragment;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OGLPP");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Sphere with Three Rotating Lights Programmable Pipeline"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();

				if (gbQKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	static WORD xMouse;
	static WORD yMouse;

	bool static bIsAKeyPressed = false;
	bool static bIsLKeyPressed = false;
	bool static bIsVKeyPressed = false;
	bool static bIsFKeyPressed = false;

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		case 0x51: // Q Key
			gbQKeyIsPressed = true;
			break;

		case 0x4C: // L key
			if (!bIsLKeyPressed) {
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else {
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;

		case 0x46: // F key
			if (!bIsFKeyPressed) {
				gbPerFragment = true;
				bIsFKeyPressed = true;
				gbPerVertex = false;
			}
			else {
				gbPerFragment = false;
				bIsFKeyPressed = false;
			}
			break;

		case 0x56: // V key
			if (!bIsVKeyPressed) {
				gbPerVertex = true;
				bIsVKeyPressed = true;
				gbPerFragment = false;
			}
			else {
				gbPerVertex = false;
				bIsVKeyPressed = false;
			}
			break;

		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void uninitialize(void);
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	/************Vertex Shader start***************/

	// Create vertex shader
	gpvVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// Provide the shader with the source code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled, u_perVertex, u_perFragment;" \
		"uniform int u_LKeyPressed;" \
		"uniform vec3 u_La, u_Ld, u_Ls;" \
		"uniform vec3 u_La1, u_Ld1, u_Ls1;" \
		"uniform vec3 u_La2, u_Ld2, u_Ls2;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform vec4 u_light_position, u_light_position1, u_light_position2;" \
		"uniform float u_material_shininess;" \
		"out vec3 out_phong_ads_color;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction, light_direction1, light_direction2;" \
		"out vec3 viewer_vector;" \

		"vec3 calculateLight(vec3 u_La, vec3 u_Ld, vec3 u_Ls, vec4 u_light_position)" \
		"{" \
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_direction = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
		"float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
		"vec3 ambient = u_La * u_Kd;" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
		"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" \
		"out_phong_ads_color = ambient + diffuse + specular;" \
		"return out_phong_ads_color;" \
		"}" \
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"if(u_perVertex == 1)" \
		"{" \
		"vec3 light0 = calculateLight(u_La, u_Ld, u_Ls, u_light_position);" \
		"vec3 light1 = calculateLight(u_La1, u_Ld1, u_Ls1, u_light_position1);" \
		"vec3 light2 = calculateLight(u_La2, u_Ld2, u_Ls2, u_light_position2);" \
		"out_phong_ads_color += light0 + light1 + light2;" \
		"}" \

		"else if(u_perFragment == 1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"viewer_vector = normalize(-eye_coordinates.xyz);" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;" \
		"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;" \
		"}" \

		"else" \
		"{" \
		"out_phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"}" \

		"else" \
		"{" \
		"out_phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \

		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gpvVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// Compile the shader
	glCompileShader(gpvVertexShaderObject);

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gpvVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gpvVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gpvVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/***********Vertex Shader end*****************/

	/**********Fragment Shader Start***********/
	// Create fragment shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Provide the shader with the source code
	GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction, light_direction1, light_direction2;" \
		"in vec3 viewer_vector;" \
		"in vec3 out_phong_ads_color;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La, u_Ld, u_Ls;" \
		"uniform vec3 u_La1, u_Ld1, u_Ls1;" \
		"uniform vec3 u_La2, u_Ld2, u_Ls2;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"uniform int u_perVertex;" \
		"uniform int u_perFragment;" \
		"vec3 calculateLight(vec3 u_La, vec3 u_Ld, vec3 u_Ls, vec3 light_direction)" \
		"{" \
		"vec3 phong_ads_color;" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"return phong_ads_color;" \
		"}" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"if(u_perVertex == 1)" \
		"{" \
		"FragColor = vec4(out_phong_ads_color, 1.0);" \
		"}" \

		"else if(u_perFragment == 1)" \
		"{" \
		"vec3 light0 = calculateLight(u_La, u_Ld, u_Ls, light_direction);" \
		"vec3 light1 = calculateLight(u_La1, u_Ld1, u_Ls1, light_direction1);" \
		"vec3 light2 = calculateLight(u_La2, u_Ld2, u_Ls2, light_direction2);" \
		"phong_ads_color += light0 + light1 + light2;" \
		"FragColor = vec4(phong_ads_color, 1.0f);" \
		"}" \

		"else" \
		"{" \
		"FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);" \
		"}" \
		"}" \

		"else" \
		"{" \
		"FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);" \
		"}" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// Compile the shader
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/**********Fragment Shader End************/

	/***************Shader Program Start**********************/
	// Create shader program
	gShaderProgramObject = glCreateProgram();

	// Attach the vertex shader to the program
	glAttachShader(gShaderProgramObject, gpvVertexShaderObject);

	// Attach the Fragment shader to the program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Per-link binding of vertex shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, PND_ATTRIBUTE_VERTEX, "vPosition");

	// Per-link binding of vertex shader program object with vertex shader light attribute
	glBindAttribLocation(gShaderProgramObject, PND_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE) {
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0) {
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	/***************Shader Program End**********************/

	/*********Uniform Locations Start*********/
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_perVertex");
	F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_perFragment");

	/*******Uniform locations for Light*******/

	/********Left Light Start**********/
	// Ambience color intensity of the light
	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	// Diffuse color intensity of the light
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	// Specular color intensity of light
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
	// Light position
	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	/********Left Light End**********/

	// Ambience color intensity of the light
	La_uniform1 = glGetUniformLocation(gShaderProgramObject, "u_La1");
	// Diffuse color intensity of the light
	Ld_uniform1 = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	// Specular color intensity of light
	Ls_uniform1 = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
	// Light position
	light_position_uniform1 = glGetUniformLocation(gShaderProgramObject, "u_light_position1");

	// Ambience color intensity of the light
	La_uniform2 = glGetUniformLocation(gShaderProgramObject, "u_La2");
	// Diffuse color intensity of the light
	Ld_uniform2 = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
	// Specular color intensity of light
	Ls_uniform2 = glGetUniformLocation(gShaderProgramObject, "u_Ls2");
	// Light position
	light_position_uniform2 = glGetUniformLocation(gShaderProgramObject, "u_light_position2");

	/*******Uniform locations for Material*******/
	// Ambient reflective color intensity of material
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// Diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// Specular reflective color intensity of material
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// Shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	/*********Uniform Locations End*********/

	/***Initialization of vertices, colors, shader attribs, vbo(vertex buffer object), vao(vertex array object)****/
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// Vertex Array Object
	glGenVertexArrays(1, &gVao_Sphere);
	glBindVertexArray(gVao_Sphere);

	// Vertex Buffer Object
	/*****Vertex Buffer Object Position Start******/
	glGenBuffers(1, &gVbo_Sphere_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Sphere_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(PND_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(PND_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/*****Vertex Buffer Object Position End******/

	/*****Vertex Buffer Object Normal Start******/
	glGenBuffers(1, &gVbo_Sphere_Normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Sphere_Normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(PND_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(PND_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/*****Vertex Buffer Object Normal End******/

	/*****Vertex Buffer Object for Element Start******/
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	/*****Vertex Buffer Object for Element End******/

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Set Orthographic Matrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLfloat lightYRotateZAxis = cos(3.1415 * gLightAngleY / 180.0f) * 10.0f;
	GLfloat lightYRotateXAxis = sin(3.1415 * gLightAngleY / 180.0f) * 10.0f;

	GLfloat lightXRotateZAxis = cos(3.1415 * gLightAngleY / 180.0f) * 10.0f;
	GLfloat lightXRotateYAxis = sin(3.1415 * gLightAngleY / 180.0f) * 10.0f;

	GLfloat lightZRotateYAxis = cos(3.1415 * gLightAngleY / 180.0f) * 10.0f;
	GLfloat lightZRotateXAxis = sin(3.1415 * gLightAngleY / 180.0f) * 10.0f;

	// Start using the shaderProgramObject
	glUseProgram(gShaderProgramObject);

	if (gbLight)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		if (gbPerVertex) {
			glUniform1i(V_KeyPressed_uniform, 1);
			glUniform1i(F_KeyPressed_uniform, 0);
		}

		else if (gbPerFragment) {
			glUniform1i(F_KeyPressed_uniform, 1);
			glUniform1i(V_KeyPressed_uniform, 0);
		}

		lightPosition[0] = 0.0f;
		lightPosition[1] = lightXRotateYAxis;
		lightPosition[2] = lightXRotateZAxis;
		lightPosition[3] = 1.0f;

		lightPosition1[0] = lightYRotateXAxis;
		lightPosition1[1] = 0.0f;
		lightPosition1[2] = lightYRotateZAxis;
		lightPosition1[3] = 1.0f;

		lightPosition2[0] = lightZRotateXAxis;
		lightPosition2[1] = lightZRotateYAxis;
		lightPosition2[2] = 0.0f;
		lightPosition2[3] = 1.0f;

		// Set light properties
		glUniform3fv(La_uniform, 1, lightAmbient);
		glUniform3fv(Ld_uniform, 1, lightDiffuse);
		glUniform3fv(Ls_uniform, 1, lightSpecular);
		glUniform4fv(light_position_uniform, 1, lightPosition);

		glUniform3fv(La_uniform1, 1, lightAmbient1);
		glUniform3fv(Ld_uniform1, 1, lightDiffuse1);
		glUniform3fv(Ls_uniform1, 1, lightSpecular1);
		glUniform4fv(light_position_uniform1, 1, lightPosition1);

		glUniform3fv(La_uniform2, 1, lightAmbient2);
		glUniform3fv(Ld_uniform2, 1, lightDiffuse2);
		glUniform3fv(Ls_uniform2, 1, lightSpecular2);
		glUniform4fv(light_position_uniform2, 1, lightPosition2);

		// Set material properties
		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	/** Here goes the code for OpenGL drawing**/

	// Set ModelView and ModelViewProjection matrices to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	// Translate the triangle to make it visible
	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// Bind vao
	glBindVertexArray(gVao_Sphere);

	// Draw the sphere
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// Unbind vao
	glBindVertexArray(0);

	// Stop using the shaderProgramObject
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void update(void)
{
	gLightAngleY = gLightAngleY + 0.2f;

	if (gLightAngleY >= 360.0f)
		gLightAngleY = gLightAngleY - 360.0f;
}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	// Destroy vao
	if (gVao_Sphere) {
		glDeleteVertexArrays(1, &gVao_Sphere);
		gVao_Sphere = 0;
	}

	// Destroy Vbo position
	if (gVbo_Sphere_Position) {
		glDeleteBuffers(1, &gVbo_Sphere_Position);
		gVbo_Sphere_Position = 0;
	}

	// Destroy Vbo normal
	if (gVbo_Sphere_Normal) {
		glDeleteBuffers(1, &gVbo_Sphere_Normal);
		gVbo_Sphere_Normal = 0;
	}

	// Detach vertex shader from shaderProgramObject
	glDetachShader(gShaderProgramObject, gpvVertexShaderObject);

	// Detach fragment shader from shaderObjectProgram
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gpvVertexShaderObject);
	gpvVertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete shaderProgramObject
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// Unlink the shader program
	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
