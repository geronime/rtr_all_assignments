#include<stdio.h>

using namespace std;

class Sphere{

private:
	void normalizeVector(float[]);
	
	boolean isFoundIdentical(final float, final float, final float);

public:
	Sphere();

	void getSphereVertexData(float [], float [], float [], short[]);
	
	int getNumberOfSphereVertices();
	
	int getNumberOfSphereElements();
	
	void processSphereData();
	
	void addTriangle(float[][], float[][], float[][])
	
}

