#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

using namespace std;

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;

int giFlag = -1;
int gWin_Width;
int gWin_Height;

GLXContext gGLXContext;

int main(void){
	void Initialize(void);
	void CreateWindow(void);
	void display(void);	
	void ToggleFullscreen(void);
	void Uninitialize(void);
	void resize(int, int);

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;

	CreateWindow();

	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone == false){
		while(XPending(gpDisplay)){
			XNextEvent(gpDisplay, &event);
			switch(event.type){
				case MapNotify:
					break;
			
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode,0,0);
					switch(keysym){
						case XK_Escape:
							bDone = true;
						break;

						case XK_F:
						case XK_f:
							if(bFullscreen == false){
								ToggleFullscreen();
								bFullscreen = true;
							}
							else{
								ToggleFullscreen();
								bFullscreen = false;
							}
						break;
						case 0x31: // 1 key
							giFlag = 1;
							resize(giWindowWidth, giWindowHeight);			
							break;

						case 0x32: // 2 key
							giFlag = 2;
							resize(giWindowWidth, giWindowHeight);			
							break;

						case 0x33: // 3 key
							giFlag = 3;
							resize(giWindowWidth, giWindowHeight);			
							break;

						case 0x34: // 4 key
							giFlag = 4;
							resize(giWindowWidth, giWindowHeight);			
							break;

						case 0x35: // 5 key
							giFlag = 5;
							resize(giWindowWidth, giWindowHeight);			
							break;

						case 0x36: // 6 key
							giFlag = 6;
							resize(giWindowWidth, giWindowHeight);			
							break;

						case 0x37: // 7 key
							giFlag = 7;
							resize(giWindowWidth, giWindowHeight);			
							break;

						case 0x38: // 8 key
							giFlag = 8;
							resize(giWindowWidth, giWindowHeight);			
							break;

						case 0x39: // 9 key
							giFlag = 9;
							resize(giWindowWidth, giWindowHeight);			
							break;
						default:
							break;
					}
				break;

				case ButtonPress:
					switch(event.xbutton.button){
						case 1:
						break;
						
						case 2:
						break;
	
						case 3:
						break;
	
						default:
						break;
					}
				break;

				case MotionNotify:
				break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					giWindowWidth = winWidth;
					giWindowHeight = winHeight;
					resize(winWidth, winHeight);
				break;

				case Expose:
				break;

				case DestroyNotify:
				break;

				case 33:
					Uninitialize();
					exit(0);
				default:
					break;
			}
		}		
		display();
	}
	Uninitialize();
	return(0);
}

void CreateWindow(void){
	void Uninitialize(void);
	
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	static int frameBufferAttributes[]=
	{
		GLX_RGBA,GLX_DOUBLEBUFFER, True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL){
		printf("Error: Unable to open X Display....\nExiting now");
		Uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo=glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,RootWindow(gpDisplay, gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if(!gWindow){
		printf("Error : Failed to Create Main Window\nExitting Now...");
		Uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "XWindowOGL - Viewport Manipulation");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete,1);

	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void){
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE", False);
	memset(&xev,0,sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&xev);
}

void Initialize(void){
	void resize(int, int);

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	
}

void display(void){
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW); // select modelview matrix
	glLoadIdentity(); // reset current modelview matrix

	/*Triangle*/
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();
	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height){
	if(height == 0)
		height = 1;

	glViewport((GLsizei)width / 4, (GLsizei)height / 3, (GLsizei)width / 2, (GLsizei)height / 2);

	/*Top left*/
	if (giFlag == 1) {
		glViewport(0, (GLsizei)height / 2, (GLsizei)width / 2, (GLsizei)height / 2);
	}

	/*Top right*/
	else if (giFlag == 2) {
		glViewport((GLsizei)width / 2, (GLsizei)height / 2, (GLsizei)width / 2, (GLsizei)height / 2);
	}	

	/*Bottom right*/
	else if (giFlag == 3) {
		glViewport((GLsizei)width / 2, 0, (GLsizei)width / 2, (GLsizei)height / 2);
	}

	/*Bottom left*/
	else if (giFlag == 4) {
		glViewport(0, 0, (GLsizei)width / 2, (GLsizei)height / 2);
	}

	/*Left*/
	else if (giFlag == 5) {
		glViewport(0, (GLsizei)height / 3, (GLsizei)width / 2, (GLsizei)height / 2);
	}

	/*Top*/
	else if (giFlag == 6) {
		glViewport((GLsizei)width / 4, (GLsizei)height / 2, (GLsizei)width / 2, (GLsizei)height / 2);
	}

	/*Right*/
	else if (giFlag == 7) {
		glViewport((GLsizei)width / 2, (GLsizei)height / 3, (GLsizei)width / 2, (GLsizei)height / 2);
	}

	/*Top*/
	else if (giFlag == 8) {
		glViewport((GLsizei)width / 4, 0, (GLsizei)width / 2, (GLsizei)height / 2);
	}

	/*Center*/
	else if (giFlag == 9) {
		glViewport((GLsizei)width / 4, (GLsizei)height / 3, (GLsizei)width / 2, (GLsizei)height / 2);
	}
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void Uninitialize(void){
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext){
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext){
		glXDestroyContext(gpDisplay, gGLXContext);
	}

	if(gWindow){
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap){
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo){
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay){
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
























