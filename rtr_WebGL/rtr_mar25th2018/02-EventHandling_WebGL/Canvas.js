// global variables
var canvas=null;
var context=null;

// onload function
function main()
{
    // Get canvas element
    var canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    // Print canvas width and height on the console
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    // Get 2D context
    var context=canvas.getContext("2d");
    if(!context)
        console.log("Obtaining 2D Context Failed\n");
    else
        console.log("Obtaining 2D Context Succeeded\n");
    
    // Fill the canvas with black color
    context.fillStyle="black"; // "#000000"
    context.fillRect(0,0,canvas.width,canvas.height);
    
    // Align the text to center
    context.textAlign="center"; // center horizontally
    context.textBaseline="middle"; // center vertically

    // TExt to display
    var str="Hello World !!!";
    
    // Font for the text
    context.font="48px sans-serif";
    
    // Color of the text
    context.fillStyle="white"; // "#FFFFFF"
    
    // Align the text to center of canvas
    context.fillText(str,canvas.width/2,canvas.height/2);
    
    // Keyboard keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
}

function keyDown(event)
{    
    alert("Key Is Pressed");
}

function mouseDown()
{    
    alert("Mouse Is Clicked");
}
