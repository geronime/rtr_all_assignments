// Global variables
var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// To start animation we need to have reuqestAnimationFrame() and check browser compatibility
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ;

// To stop animation we need to have cancelAnimationFrame() and check browser compatibility
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCanceRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ;

function main(){
	
	// Get canvas
	canvas = document.getElementById("AMC");
	
	if(!canvas){
		console.log("Unable to obtain canvas!!");
	}
	else{
		console.log("Canvas obtained successfully");			
	}
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register event handlers
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	// WebGL initialization
	init();
	
	resize();
	draw();
}

function toggleFullScreen(){
	
	var fullscreen_element =
	document.fullscreenElement ||
	document.webkitFullScreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	// If not fullscreen
	if(fullscreen_element == null){
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullScreen)
			canvas.msRequestFullScreen();
		
		bFullScreen = true;
	}
	else{ // If already fullscreen
		if(document.exitFullScreen)
			document.exitFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullScreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullScreen = false;
	}
}

function init(){
	
	// Get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	
	if(gl == null){
		console.log("Failsed to get WebGL rendering context");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	// Set the clear color	
	gl.clearColor(0.0, 0.0, 1.0, 1.0); // Blue screen
}

function resize(){
	
	if(bFullScreen){
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// Set the viewport to match original state
	gl.viewport(0, 0, canvas.width, canvas.height);
}

function draw(){
	
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	// Animation loop
	requestAnimationFrame(draw, canvas);
}

function keyDown(event){
	
	switch(event.keyCode){
		case 70: // 'F' or 'f'
			toggleFullScreen();
			break;
	}
}

function mouseDown(){
	
	
}