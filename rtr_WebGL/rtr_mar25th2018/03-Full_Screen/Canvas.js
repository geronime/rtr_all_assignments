// Global variables
var canvas=null;
var context=null;

function main()
{
    // Get canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
        
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    // get 2D context
    context=canvas.getContext("2d");
    if(!context)
        console.log("Obtaining 2D Context Failed\n");
    else
        console.log("Obtaining 2D Context Succeeded\n");
    
    // Canvas "black" color
    context.fillStyle="black"; // "#000000"
    context.fillRect(0,0,canvas.width,canvas.height);
    
    // Draw text
    drawText("Hello World !!!");
    
    // Register event handlers
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
}

function drawText(text)
{
    
    // Align the text in center
    context.textAlign="center"; // Horizontally
    context.textBaseline="middle"; // Vertically
    
    // Font
    context.font="48px sans-serif";
    
    // Text color
    context.fillStyle="white"; // "#FFFFFF"
    
    // Display text in center
    context.fillText(text,canvas.width/2,canvas.height/2);
}

function toggleFullScreen()
{
    
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // If not fullscreen
    if(fullscreen_element==null){
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
    }
    else{ // If already fullscreen
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
    }
}

function keyDown(event)
{
    
    switch(event.keyCode)
    {
        case 70: // 'F' or 'f'
            toggleFullScreen();
            
            // Repaint
            drawText("Hello World !!!");
            break;
    }
}

function mouseDown()
{
    
}
