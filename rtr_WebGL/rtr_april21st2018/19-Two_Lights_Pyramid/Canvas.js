// global variables
var canvas=null;
var gl=null; // WebGL context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all elements inside are rendered as 'const'
{
PND_ATTRIBUTE_VERTEX:0,
PND_ATTRIBUTE_COLOR:1,
PND_ATTRIBUTE_NORMAL:2,
PND_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light_ambient_left=[0.0,0.0,0.0];
var light_diffuse_left=[1.0,0.0,0.0];
var light_specular_left=[1.0,1.0,1.0];
var light_position_left=[-2.0,0.0,0.0,1.0];

var light_ambient_right=[0.0,0.0,0.0];
var light_diffuse_right=[0.0,0.0,1.0];
var light_specular_right=[1.0,1.0,1.0];
var light_position_right=[2.0,0.0,0.0,1.0];

var material_ambient= [0.0,0.0,0.0];
var material_diffuse= [1.0,1.0,1.0];
var material_specular= [1.0,1.0,1.0];
var material_shininess= 50.0;

var vao_pyramid;
var vbo_position;
var vbo_normals;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform_right, ldUniform_right, lsUniform_right, lightPositionUniform_right;
var laUniform_left, ldUniform_left, lsUniform_left, lightPositionUniform_left;
var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var LKeyPressedUniform;

var bLKeyPressed=false;

var anglePyramid = 0.0;

var perspectiveProjectionMatrix;

// To start animation we need to have reuqestAnimationFrame() and check browser compatibility
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation we need to have cancelAnimationFrame() and check browser compatibility
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main()
{
    // Get canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // Register event handlers
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // WebGL initialization
    init();
    
    resize();
    draw();
}

function toggleFullScreen()
{
    
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // If not fullscreen
    if(fullscreen_element==null){
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        
		bFullscreen=true;
    }
    else{ // if already fullscreen
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        
		bFullscreen=false;
    }
}

function init()
{
    // Get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null)
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
    
    // Vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;"+
    "uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position1, u_light_position2;"+
    "out vec3 transformed_normals;"+
    "out vec3 light_direction1, light_direction2;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
    "light_direction1 = vec3(u_light_position1) - eye_coordinates.xyz;"+
	"light_direction2 = vec3(u_light_position2) - eye_coordinates.xyz;"+
    "viewer_vector = -eye_coordinates.xyz;"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}";
    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // Fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec3 transformed_normals;"+
    "in vec3 light_direction1, light_direction2;"+
    "in vec3 viewer_vector;"+
    "out vec4 FragColor;"+
    "uniform vec3 u_La1, u_La2;"+
    "uniform vec3 u_Ld1, u_Ld2;"+
    "uniform vec3 u_Ls1, u_Ls2;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
    "vec3 calculateLight(vec3 u_La, vec3 u_Ld, vec3 u_Ls, vec3 light_direction)"+
	"{"+
    "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
    "vec3 normalized_light_direction=normalize(light_direction);"+
    "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
    "vec3 ambient = u_La * u_Ka;"+
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
    "vec3 phong_ads_color=ambient + diffuse + specular;"+
	"return phong_ads_color;"+
    "}"+
	"void main(void)"+
    "{"+
    "vec3 phong_ads_color;"+
    "if(u_LKeyPressed == 1)"+
    "{"+
	"vec3 light1 = calculateLight(u_La1, u_Ld1, u_Ls1, light_direction1);"+
	"vec3 light2 = calculateLight(u_La2, u_Ld2, u_Ls2, light_direction2);"+
	"phong_ads_color += light1 + light2;"+
	"}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
    "}"+
    "FragColor = vec4(phong_ads_color, 1.0);"+
    "}"
    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.PND_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.PND_ATTRIBUTE_NORMAL,"vNormal");
    
    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get Model Matrix uniform location
    modelMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    viewMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
    // get Projection Matrix uniform location
    projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
    
    // get single tap detecting uniform
    LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
    
	// Left light
    // ambient color intensity of light
    laUniform_left=gl.getUniformLocation(shaderProgramObject,"u_La1");
    // diffuse color intensity of light
    ldUniform_left=gl.getUniformLocation(shaderProgramObject,"u_Ld1");
    // specular color intensity of light
    lsUniform_left=gl.getUniformLocation(shaderProgramObject,"u_Ls1");
    // position of light
    lightPositionUniform_left=gl.getUniformLocation(shaderProgramObject,"u_light_position1");
    
	// Right light
	// ambient color intensity of light
    laUniform_right=gl.getUniformLocation(shaderProgramObject,"u_La2");
    // diffuse color intensity of light
    ldUniform_right=gl.getUniformLocation(shaderProgramObject,"u_Ld2");
    // specular color intensity of light
    lsUniform_right=gl.getUniformLocation(shaderProgramObject,"u_Ls2");
    // position of light
    lightPositionUniform_right=gl.getUniformLocation(shaderProgramObject,"u_light_position2");
	
    // ambient reflective color intensity of material
    kaUniform=gl.getUniformLocation(shaderProgramObject,"u_Ka");
    // diffuse reflective color intensity of material
    kdUniform=gl.getUniformLocation(shaderProgramObject,"u_Kd");
    // specular reflective color intensity of material
    ksUniform=gl.getUniformLocation(shaderProgramObject,"u_Ks");
    // shininess of material ( value is conventionally between 1 to 200 )
    materialShininessUniform=gl.getUniformLocation(shaderProgramObject,"u_material_shininess");
    
    // *** vertices, colors, shader attribs, vbo_position, vao_pyramid initializations ***
    var pyramidVertices=new Float32Array([
											0.0, 1.0, 0.0,
											-1.0, -1.0, 1.0,
											1.0, -1.0, 1.0,

											0.0, 1.0, 0.0,
											1.0,-1.0, 1.0,
											1.0, -1.0, -1.0,

											0.0, 1.0, 0.0,
											1.0, -1.0, -1.0,
											-1.0, -1.0, -1.0,

											0.0, 1.0, 0.0,
											-1.0, -1.0,-1.0,
											-1.0, -1.0, 1.0
                                           ]);

	var pyramidNormals=new Float32Array([
											0.0, 0.0, 1.0,
											0.0, 0.0, 1.0,
											0.0, 0.0, 1.0,

											1.0, 0.0, 0.0,
											1.0, 0.0, 0.0,
											1.0, 0.0, 0.0,

											0.0, 0.0, -1.0,
											0.0, 0.0, -1.0,
											0.0, 0.0, -1.0,

											-1.0, 0.0, 0.0,
											-1.0, 0.0, 0.0,
											-1.0, 0.0, 0.0
                                           ]);
	
	/************Triangle vertex array object start*************/
    vao_pyramid=gl.createVertexArray();
    gl.bindVertexArray(vao_pyramid);
    
	/*********vbo for position start*********/
    vbo_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.PND_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.PND_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
	/*********vbo for position end*********/		
	
	/*********vbo for normals start*********/
    vbo_normals = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_normals);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidNormals,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.PND_ATTRIBUTE_NORMAL,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.PND_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
	/*********vbo for normals end*********/
	
    gl.bindVertexArray(null);
	/************Triangle vertex array object end*************/	
	
	// Enable depth test
	gl.enable(gl.DEPTH_TEST);
	
	// depth test to do
	gl.depthFunc(gl.LEQUAL);
	
	// Disable back face culling
	gl.disable(gl.CULL_FACE);
	
    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // Set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    // Orthographic Projection
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
    
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    gl.useProgram(shaderProgramObject);
    
	/********Pyramid drawing start********/
    if(bLKeyPressed==true)
    {
        gl.uniform1i(LKeyPressedUniform, 1);
        
        // setting light properties
        gl.uniform3fv(laUniform_left, light_ambient_left); // ambient intensity of light
        gl.uniform3fv(ldUniform_left, light_diffuse_left); // diffuse intensity of light
        gl.uniform3fv(lsUniform_left, light_specular_left); // specular intensity of light
        gl.uniform4fv(lightPositionUniform_left, light_position_left); // light position
        
		gl.uniform3fv(laUniform_right, light_ambient_right); // ambient intensity of light
        gl.uniform3fv(ldUniform_right, light_diffuse_right); // diffuse intensity of light
        gl.uniform3fv(lsUniform_right, light_specular_right); // specular intensity of light
        gl.uniform4fv(lightPositionUniform_right, light_position_right); // light position
		
        // setting material properties
        gl.uniform3fv(kaUniform, material_ambient); // ambient reflectivity of material
        gl.uniform3fv(kdUniform, material_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(ksUniform, material_specular); // specular reflectivity of material
        gl.uniform1f(materialShininessUniform, material_shininess); // material shininess
    }
    else
    {
        gl.uniform1i(LKeyPressedUniform, 0);
    }
    
    var modelMatrix=mat4.create();
    var viewMatrix=mat4.create();

    mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.0]);
	
	mat4.rotateY(modelMatrix, modelMatrix, degToRad(anglePyramid));	    
    
    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);
	
    gl.bindVertexArray(vao_pyramid);

    gl.drawArrays(gl.TRIANGLES,0,12);
    
    gl.bindVertexArray(null);
	/********Pyramid drawing end********/    	
	
    gl.useProgram(null);
    
	// Call to update
	update();
	
    // animation loop
    requestAnimationFrame(draw, canvas);
}

function update() {
	anglePyramid = anglePyramid + 1.0;
	if (anglePyramid >= 360.0) {
		anglePyramid = 0.0;
	}	
}

function degToRad(degree){
	return degree * (Math.PI/180.0);
}

function uninitialize()
{
    // code
    if(vao_pyramid)
    {
        gl.deleteVertexArray(vao_pyramid);
        vao_pyramid=null;
    }
    
    if(vbo_position)
    {
        gl.deleteBuffer(vbo_position);
        vbo_position=null;
    }
	
	if(vbo_color)
    {
        gl.deleteBuffer(vbo_color);
        vbo_color=null;
    }
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
        case 76: // for 'L' or 'l'
            if(bLKeyPressed==false)
                bLKeyPressed=true;
            else
                bLKeyPressed=false;
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}

function mouseDown()
{
    // code
}