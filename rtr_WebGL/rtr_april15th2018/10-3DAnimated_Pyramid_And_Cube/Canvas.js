// global variables
var canvas=null;
var gl=null; // WebGL context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all elements inside are rendered as 'const'
{
PND_ATTRIBUTE_VERTEX:0,
PND_ATTRIBUTE_COLOR:1,
PND_ATTRIBUTE_NORMAL:2,
PND_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_triangle;
var vao_square;
var vbo_position;
var vbo_color;
var mvpUniform;

var angleTri = 0.0;
var angleSquare = 0.0;

var perspectiveProjectionMatrix;

// To start animation we need to have reuqestAnimationFrame() and check browser compatibility
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation we need to have cancelAnimationFrame() and check browser compatibility
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main()
{
    // Get canvas
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // Register event handlers
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // WebGL initialization
    init();
    
    resize();
    draw();
}

function toggleFullScreen()
{
    
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // If not fullscreen
    if(fullscreen_element==null){
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        
		bFullscreen=true;
    }
    else{ // if already fullscreen
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        
		bFullscreen=false;
    }
}

function init()
{
    // Get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null)
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
    
    // Vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
	"in vec4 vColor;"+
	"out vec4 out_Color;"+
    "uniform mat4 u_mvp_matrix;"+
    "void main(void)"+
    "{"+
    "gl_Position = u_mvp_matrix * vPosition;"+
	"out_Color = vColor;"+
    "}";
    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // Fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
	"in vec4 out_Color;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = out_Color;"+
    "}"
    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.PND_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.PND_ATTRIBUTE_COLOR,"vColor");
    
    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get MVP uniform location
    mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
    
    // *** vertices, colors, shader attribs, vbo_position, vao_triangle initializations ***
    var pyramidVertices=new Float32Array([
											0.0, 1.0, 0.0,
											-1.0, -1.0, 1.0,
											1.0, -1.0, 1.0,
											0.0, 1.0, 0.0,
											1.0, -1.0, 1.0,
											1.0, -1.0, -1.0,
											0.0, 1.0, 0.0,
											1.0, -1.0, -1.0,
											-1.0, -1.0, -1.0,
											0.0, 1.0, 0.0,
											-1.0, -1.0, -1.0,
											-1.0, -1.0, 1.0
                                           ]);
	
	var pyramidColors=new Float32Array([
										1.0, 0.0, 0.0,
										0.0, 1.0, 0.0,
										0.0, 0.0, 1.0,
										1.0, 0.0, 0.0,
										0.0, 0.0, 1.0,
										0.0, 1.0, 0.0,
										1.0, 0.0, 0.0,
										0.0, 1.0, 0.0,
										0.0, 0.0, 1.0,
										1.0, 0.0, 0.0,
										0.0, 0.0, 1.0,
										0.0, 1.0, 0.0
										]);
	
	var cubeVertices=new Float32Array([
										// Front Face
										-1.0, 1.0, 1.0,
										-1.0, -1.0, 1.0,
										1.0, -1.0, 1.0,
										1.0, 1.0, 1.0,

										// Back Face
										-1.0, 1.0, -1.0,
										-1.0, -1.0, -1.0,
										1.0, -1.0, -1.0,
										1.0, 1.0, -1.0,

										// Right Face
										1.0, 1.0, 1.0,
										1.0, 1.0, -1.0,
										1.0, -1.0, -1.0,
										1.0, -1.0, 1.0,

										// Left face
										-1.0, 1.0, 1.0,
										-1.0, 1.0, -1.0,
										-1.0, -1.0, -1.0,
										-1.0, -1.0, 1.0,

										// Top Face
										- 1.0, 1.0, 1.0,
										-1.0, 1.0, -1.0,
										1.0, 1.0, -1.0,
										1.0, 1.0, 1.0,

										// Bottom Face
										-1.0, -1.0, 1.0,
										-1.0, -1.0, -1.0,
										1.0, -1.0, -1.0,
										1.0, -1.0, 1.0
										]);
	
	var cubeColors=new Float32Array([
									1.0, 0.0, 0.0,
									1.0, 0.0, 0.0,
									1.0, 0.0, 0.0,
									1.0, 0.0, 0.0,

									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,

									0.0, 1.0, 0.0,
									0.0, 1.0, 0.0,
									0.0, 1.0, 0.0,
									0.0, 1.0, 0.0,

									1.0, 1.0, 0.0,
									1.0, 1.0, 0.0,
									1.0, 1.0, 0.0,
									1.0, 1.0, 0.0,

									1.0, 0.0, 1.0,
									1.0, 0.0, 1.0,
									1.0, 0.0, 1.0,
									1.0, 0.0, 1.0,

									0.0, 1.0, 1.0,
									0.0, 1.0, 1.0,
									0.0, 1.0, 1.0,
									0.0, 1.0, 1.0
									]);
	
	/************Triangle vertex array object start*************/
    vao_triangle=gl.createVertexArray();
    gl.bindVertexArray(vao_triangle);
    
	/*********vbo for position start*********/
    vbo_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.PND_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.PND_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
	/*********vbo for position end*********/
	
	/*********vbo for color start*********/
	vbo_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color);
    gl.bufferData(gl.ARRAY_BUFFER,pyramidColors,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.PND_ATTRIBUTE_COLOR,
                           3, 
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.PND_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
	/*********vbo for color end*********/
	
    gl.bindVertexArray(null);
	/************Triangle vertex array object end*************/
	
	/************Square vertex array object start*************/
    vao_square=gl.createVertexArray();
    gl.bindVertexArray(vao_square);
    
	/*********vbo for position start*********/
    vbo_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
    gl.bufferData(gl.ARRAY_BUFFER,cubeVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.PND_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.PND_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
	/*********vbo for position end*********/
	
	/*********vbo for color start*********/
    vbo_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color);
    gl.bufferData(gl.ARRAY_BUFFER,cubeColors,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.PND_ATTRIBUTE_COLOR,
                           3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.PND_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
	/*********vbo for color end*********/
	
    gl.bindVertexArray(null);
	/************Square vertex array object end*************/
	
	// Enable depth test
	gl.enable(gl.DEPTH_TEST);
	
	// depth test to do
	gl.depthFunc(gl.LEQUAL);
	
	// Disable back face culling
	gl.disable(gl.CULL_FACE);
	
    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // Set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    // Orthographic Projection
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
    
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    gl.useProgram(shaderProgramObject);
    
	/********Triangle drawing start********/
    var modelViewMatrix=mat4.create();
    var modelViewProjectionMatrix=mat4.create();
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -5.0]);
	
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleTri));
	
    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	
    gl.bindVertexArray(vao_triangle);

    gl.drawArrays(gl.TRIANGLES,0,12);
    
    gl.bindVertexArray(null);
	/********Triangle drawing end********/
    
	/********Square drawing start********/
    modelViewMatrix=mat4.create();
    modelViewProjectionMatrix=mat4.create();
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -5.0]);
	
	mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleSquare));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleSquare));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleSquare));
	
    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(vao_square);

    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.drawArrays(gl.TRIANGLE_FAN,16,4);
	gl.drawArrays(gl.TRIANGLE_FAN,20,4);
    
    gl.bindVertexArray(null);
	/********Square drawing end********/
	
    gl.useProgram(null);
    
	// Call to update
	update();
	
    // animation loop
    requestAnimationFrame(draw, canvas);
}

function update() {
	angleTri = angleTri + 1.0;
	if (angleTri >= 360.0) {
		angleTri = 0.0;
	}

	angleSquare = angleSquare + 1.0;
	if (angleSquare >= 360.0) {
		angleSquare = 0.0;
	}
}

function degToRad(degree){
	return degree * (Math.PI/180.0);
}

function uninitialize()
{
    // code
    if(vao_triangle)
    {
        gl.deleteVertexArray(vao_triangle);
        vao_triangle=null;
    }
    
    if(vbo_position)
    {
        gl.deleteBuffer(vbo_position);
        vbo_position=null;
    }
	
	if(vbo_color)
    {
        gl.deleteBuffer(vbo_color);
        vbo_color=null;
    }
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
			gBrowser.removeCurrentTab();
            //window.close(); // may not work in Firefox but works in Safari and chrome
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}

function mouseDown()
{
    // code
}